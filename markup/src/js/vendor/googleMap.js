function InitMap() {
    let map,
        addressArray = [],
        markers = [],
        here,
        mapsCard;
        const locationButton = document.getElementById("button");

        

    this.init = function () {
        if($(".pg--store").length) {

            map = new google.maps.Map(document.getElementById("map"), {
                center: { lat: 41.3099786206113, lng: 69.27868360279695 },
                zoom: 10,
            });

            function takeAttrData() {
                addressArray = [];
                mapsCard = $(".js-address-item")
                mapsCard.each(function (indx, element) {
                    let lat = $(this).attr("data-address-lat");
                    let lng = $(this).attr("data-address-lng");
                    addressArray.push({
                        lat: lat,
                        lng: lng,
                    });
                });
            }

            function addMarkerMap() {
                for (let i = 0; i < addressArray.length; i++) {
                    const marker = new google.maps.Marker({
                        position: {
                            lat: +addressArray[i].lat,
                            lng: +addressArray[i].lng,
                        },
                        map,
                    });
                    markers.push(marker);
                }
            }

            function setMapOnAll(map) {
                for (let i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
                markers = [];
            }

            infoWindow = new google.maps.InfoWindow();

            takeAttrData();
            addMarkerMap();

            locationButton.addEventListener("click", (e) => {
                e.preventDefault();
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            here = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };

                            let optimalAddress = [];
                            let numb = 0;

                            for (let i = 0; i < addressArray.length; i++) {
                                let a, b, c;

                                a = Math.pow(
                                    Math.abs(+here.lat - +addressArray[i].lat),
                                    2
                                );
                                b = Math.pow(
                                    Math.abs(+here.lng - +addressArray[i].lng),
                                    2
                                );
                                c = Math.sqrt(a + b);
                                if (c > numb && numb == 0) {
                                    numb = c;
                                    optimalAddress.splice(0);
                                    optimalAddress.push(
                                        addressArray[i].lat,
                                        addressArray[i].lng
                                    );
                                } else if (c < numb) {
                                    numb = c;
                                    optimalAddress.splice(0);
                                    optimalAddress.push(
                                        addressArray[i].lat,
                                        addressArray[i].lng
                                    );
                                }
                            }

                            const pos = {
                                lat: +optimalAddress[0],
                                lng: +optimalAddress[1],
                            };

                            mapsCard.each(function (indx, element) {
                                if (
                                    $(this).attr("data-address-lat") !=
                                        optimalAddress[0] &&
                                    $(this).attr("data-address-lng") !=
                                        optimalAddress[1]
                                ) {
                                    $(this).remove();
                                }
                            });
                            window.addressSwiper.updateSlides()
                            infoWindow.open(map);
                            map.setCenter(pos);
                            map.setZoom(15);
                        },
                        () => {
                            handleLocationError(true, infoWindow, map.getCenter());
                        }
                    );
                } else {
                    handleLocationError(false, infoWindow, map.getCenter());
                }
                return false;
            });
            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(
                    browserHasGeolocation
                        ? "Error: The Geolocation service failed."
                        : "Error: Your browser doesn't support geolocation."
                );
                infoWindow.open(map);
            }
        }
    };

    this.addMarker = function () {
        function takeAttrData() {
            addressArray = [];
            mapsCard = $(".js-address-item")
            mapsCard.each(function (indx, element) {
                let lat = $(this).attr("data-address-lat");
                let lng = $(this).attr("data-address-lng");
                addressArray.push({
                    lat: lat,
                    lng: lng,
                });
            });
        }

        function addMarkerMap() {
            for (let i = 0; i < addressArray.length; i++) {
                const marker = new google.maps.Marker({
                    position: {
                        lat: +addressArray[i].lat,
                        lng: +addressArray[i].lng,
                    },
                    map,
                });
                markers.push(marker);
            }
        }

        function setMapOnAll(map) {
            for (let i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
            markers = [];
        }
        setMapOnAll(null);
        takeAttrData();
        addMarkerMap();
        return false;
    };
}
