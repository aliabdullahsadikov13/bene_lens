from aiogram.types import Message, ReplyKeyboardRemove

from keyboards.default.AddressOflayn import ToshkentUz, BackButton_uz
from loader import dp, db, bot

from utils.misc import show_on_gmaps



#Toshkent shahri tumanlari bo`yicha
@dp.message_handler(text="Butun shahar 🏙")
async def chilonzor(message: Message):
    stores = await db.select_storeByRegion_uz("Toshkent shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Toshkent shahrida {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz)
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)




#Toshkent shahri tumanlari bo`yicha
@dp.message_handler(text="Mirzo Ulug`bek tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Mirzo Ulug`bek tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz)
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz
                             )

@dp.message_handler(text="Mirobod tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Mirobod tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz)
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz
                             )


@dp.message_handler(text="Shayxontohur tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Shayxontohur tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store[0]}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Olmazor tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Olmazor tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Yunusobod tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yunusobod tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Sergeli tumani")
async def sergeli(message: Message):
    stores = await db.select_store_uz("Sergeli tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Yakkasaroy tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yakkasaroy tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Chilonzor tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Chilonzor tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Uchtepa tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Uchtepa tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Bektemir tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Bektemir tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Yashnobod tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yashnobod tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)

#Andijon viloyati tumanlari bo`yicha

@dp.message_handler(text="Shaxrixon tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Shaxrixon tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store[0]}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Xo`jaobod tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Xo`jaobod tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Oltinko`l tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Oltinko`l tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Baliqchi tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Baliqchi tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Izboskan tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Izboskan tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Asaka tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Asaka tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Jalaquduq tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Jalaquduq tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Andijon tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Andijon tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Andijon shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Andijon shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


# Buxoro viloyati tumanlari bo`yicha

@dp.message_handler(text="Peshku tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Peshku tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Buxoro tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Buxoro tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Kogon tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Kogon tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Qorako`l tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qorako`l tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Romitan tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Romitan tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Buxoro shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Buxoro shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


# Farg`ona viloyati tumanlari bo`yicha


@dp.message_handler(text="Quva tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Quva tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Bog`dod tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Bog`dod tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="O'zbekiston tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("O'zbekiston tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Farg`ona tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Farg`ona tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Yozyovon tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yozyovon tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Rishton tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Rishton tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Uchko`prik tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Uchko`prik tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Dang`ara tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Dang`ara tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Farg`ona shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Farg`ona shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Qo`qon shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qo`qon shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)

# Jizzax viloyati tumanlari bo`yicha

@dp.message_handler(text="Jizzax shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Jizzax shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Forish tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Forish tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


# Jizzax viloyati tumanlari bo`yicha

@dp.message_handler(text="Chortoq tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Chortoq tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Kosonsoy tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Kosonsoy tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Uchqo`rg`on tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Uchqo`rg`on tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Yangiqo`rg`on tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yangiqo`rg`on tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


#Navoiy viloyati tumanlari bo`yicha


@dp.message_handler(text="Qiziltepa tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qiziltepa tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Karmana tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Karmana tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Navoiy shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Navoiy shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


# Qashqadaryo viloyati tumanlari bo`yicha

@dp.message_handler(text="Qarshi shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qarshi shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Koson tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Koson tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Chiroqchi tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Chiroqchi tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Shahrisabz tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Shahrisabz tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


#Qoraqalpog`iston respublikasi tumanlari bo`yicha

@dp.message_handler(text="Nukus shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Nukus shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="To`rtko`l tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("To`rtko`l tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Qo`ng`irot tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qo`ng`irot tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Xo`jayli tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Xo`jayli tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


#Samarqand viloyati tumanlari bo`yicha

@dp.message_handler(text="Payariq tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Payariq tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Qo`shrabot tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qo`shrabot tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Samarqand tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Samarqand tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Urgut tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Urgut tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Samarqand shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Samarqand shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)

@dp.message_handler(text="Bulung`ur tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Bulung`ur tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


#Sirdaryo viloyati tumanlari bo`yicha

@dp.message_handler(text="Guliston tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Guliston tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Xovos tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Xovos tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Guliston shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Guliston shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Yangiyer tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yangiyer tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Sirdaryo tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Sirdaryo tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


#Surxondaryo viloyati tumanlari bo`yicha magazinlar ro`yhati

@dp.message_handler(text="Jarqo`rg`on tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Jarqo`rg`on tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Uzun tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Uzun tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Termiz shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Termiz shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Sho`rchi tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Sho`rchi tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



#Toshkent viloyati tumanlari bo`yicha magazinlar ro`yhati

@dp.message_handler(text="Bekobod tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Bekobod tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Qibray tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qibray tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Yangiyo`l tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yangiyo`l tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Toshkent tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Toshkent tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Bo`stonliq tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Bo`stonliq tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Zangiota tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Zangiota tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Parkent tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Parkent tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Olmaliq shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Olmaliq shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Angren shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Angren shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Bekobod shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Bekobod shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Chirchiq shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Chirchiq shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Nurafshon shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Nurafshon shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



#Xorazm viloyati tumanlari bo`yicha magazinlar ro`yhati

@dp.message_handler(text="Bog`ot tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Bog`ot tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Qo`shko`pir tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Qo`shko`pir tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Xazorasp tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Xazorasp tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Xonqa tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Xonqa tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Shovot tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Shovot tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Yangibozor tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Yangibozor tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Urganch shahri")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Urganch shahri")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



@dp.message_handler(text="Gurlan tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Gurlan tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)


@dp.message_handler(text="Urganch tumani")
async def chilonzor(message: Message):
    stores = await db.select_store_uz("Urganch tumani")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_uz']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_uz']}\n\n"
            msg += f"📍 Manzil: {store['district_uz']}, {store['street_uz']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_uz']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=BackButton_uz
                             )
    else:
        await message.answer("❌ Optika do`konlari sizning hududingizda topilmadi.\n",reply_markup=BackButton_uz)



