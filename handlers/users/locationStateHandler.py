import math

import asyncpg
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command, state
from aiogram.types import Message

from keyboards.default.MenuKeyboard import UzMenu
from keyboards.default.OfflaynKeyboard import UzOfflayn
from keyboards.default.langChooseKeyboard import langMenu
from keyboards.default.locationKeyboard import location_keyboard, location_Back_uz
from loader import dp, db
from states.locationStates import LocationData
from utils.misc import show_on_gmaps


def calc_distance(lat1, lon1, lat2, lon2):
    R = 6371000
    phi_1 = math.radians(lat1)
    phi_2 = math.radians(lat2)

    delta_phi = math.radians(lat2 - lat1)
    delta_lambda = math.radians(lon2 - lon1)

    a = math.sin(delta_phi / 2.0) ** 2 + \
        math.cos(phi_1) * math.cos(phi_2) * \
        math.sin(delta_lambda / 2.0) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    meters = R * c  # output distance in meters
    return meters / 1000.0  # output distance in kilometers



@dp.message_handler(text="📍 Manzilimni yuborish",state=None)
async def LocationSendUz(message: Message):
    await message.reply("Lokatsiya yuboring...",reply_markup=location_keyboard)
    await LocationData.location_uz.set()

@dp.message_handler(content_types='location', state=LocationData.location_uz)
async def get_location(message:types.Message, state:FSMContext):
    location = message.location
    latitude = location.latitude
    longitude = location.longitude
    Shops = []
    stores = await db.select_all_stores()
    count = 0
    for store in stores:
        my_store = []
        store_location = {'lat': '', 'lon': ''}
        store_location['lat'] = float(store['lat'])
        store_location['lon'] = float(store['lang'])
        if store['store_name_uz']:
            store_name_uz = store['store_name_uz']
        if store['store_name_ru']:
            store_name_ru = store['store_name_ru']
        if store['store_phone']:
            store_phone = store['store_phone']
        if store['region_uz']:
            store_city_uz = store['region_uz']
        if store['region_ru']:
            store_city_ru = store['region_ru']
        if store['district_uz'] or store['street_uz']:
            store_address_uz = f"{store['district_uz']}, {store['street_uz']}"
        if store['district_ru'] or store['street_ru']:
            store_address_ru = f"{store['district_ru']}, {store['street_ru']}"
        if store['store_photo']:
            store_photo = store['store_photo']
        store_url = show_on_gmaps.show(**store_location)
        my_store.extend([store_name_uz, store_name_ru, store_phone, store_city_uz, store_city_ru, store_address_uz,
                         store_address_ru, store_photo,
                         calc_distance(latitude, longitude, store_location['lat'], store_location['lon']), store_url])
        my_store = tuple(my_store)
        Shops.append(my_store)

    closest_shops = sorted(Shops, key=lambda x: x[8], reverse=True)[-10:]

    await message.answer(f"<b>Sizga yaqin bo'lgan barcha magazinlar ro'yhati bilan tanishing</b>")
    for store_name_uz, store_name_ru, shop_phone, shop_city_uz, shop_city_ru, shop_add_uz, shop_add_ru, store_photo, distance, url in closest_shops:
        count = count + 1
        photo_url = store_photo
        msg = f"👁 Optika: <b>{store_name_uz}</b> \n\n"
        msg += f"📞 Telefon: {shop_phone}\n\n"
        msg += f"🏢 Shahar: {shop_city_uz}\n\n"
        msg += f"📍 Manzil: {shop_add_uz}\n\n"
        msg += f"🏃 Lokatsiya: <a href='{url}'>{store_name_uz}</a>\n\n"
        msg += f"🚗 Masofa: {distance:.1f} km."
        await message.answer_photo(photo_url, caption=msg)
    await message.answer(f"✅ Yaqin atrofda {count} ta optika magazini topildi.\n",reply_markup=location_Back_uz)


@dp.message_handler(text="◀️Orgaga",state=LocationData.location_uz)
async def settingsUzNazad(message:types.Message,state:FSMContext):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzOfflayn)
    await state.reset_state()

@dp.message_handler(text="⏫  Bosh sahifa",state=LocationData.location_uz)
async def settingsUzNazad(message:types.Message,state:FSMContext):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzMenu)
    await state.reset_state()

@dp.message_handler(commands='start',state=LocationData.location_uz)
async def bot_start(message:types.Message,state:FSMContext):
    try:
        user = await db.add_user(telegram_id=message.from_user.id,
                                 full_name=message.from_user.full_name,
                                 username=message.from_user.username)
    except asyncpg.exceptions.UniqueViolationError:
        user = await db.select_user(telegram_id=message.from_user.id)

    await message.answer(f"👋 Salom, {message.from_user.first_name}!\n"
                         f"🇺🇿 Iltimos, menyudan mavjud tilni tanlang.\n"
                         f"🇷🇺 Пожалуйста, выберите доступный язык из меню", reply_markup=langMenu)
    await state.reset_state()


@dp.message_handler(commands='help',state=LocationData.location_uz)
async def bot_help(message:types.Message):
    text = ("Buyruqlar: ",
            "/start - Botni ishga tushirish",
            "/help - Yordam")

    await message.answer("\n".join(text))
