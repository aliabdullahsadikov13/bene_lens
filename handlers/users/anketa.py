from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command

from loader import dp
from states.personalData import PersonalData

@dp.message_handler(Command("anketa"))
async def enter_test(message:types.Message):
    await message.answer("To'liq ismingizni kiriting")
    await PersonalData.fullName.set()

@dp.message_handler(state=PersonalData.fullName)
async def answer_fullName(message:types.Message,state:FSMContext):
    fullname = message.text
    await state.update_data(
        {"name":fullname}
    )

    await message.answer("Email manzilni kiriting")
    # await PersonalData.next()
    await PersonalData.email.set()

@dp.message_handler(state=PersonalData.email)
async def answer_email(message:types.Message,state:FSMContext):
    email = message.text
    await state.update_data(
        {"email":email}
    )

    await message.answer("Telefon raqamingizni kiriting")
    # await PersonalData.next()
    await PersonalData.phoneNum.set()

@dp.message_handler(state=PersonalData.phoneNum)
async def answer_phone(message:types.Message,state:FSMContext):
    phone = message.text
    await state.update_data(
        {"phone":phone}
    )

    #ma'lumotlarni qayta o'qish
    data = await state.get_data()
    name = data.get("name")
    email = data.get("email")
    phone = data.get("phone")

    msg = "Quyidagi ma`lumotlar qabul qilindi:\n"
    msg += f"Ismingiz - {name}\n"
    msg += f"Email - {email}\n"
    msg += f"Telefon: - {phone}"
    await message.answer(msg)

    # State dan chiqaramiz
    # 1-variant
    await state.finish()

    # 2-variant
    # await state.reset_state()

    # 3-variant. Ma`lumotlarni saqlab qolgan holda
    # await state.reset_state(with_data=False)


    #this type of saving the data is not secure
