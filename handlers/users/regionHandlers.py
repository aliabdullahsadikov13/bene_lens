from aiogram.types import Message, ReplyKeyboardRemove

from keyboards.default.AddressOflayn import BackButton_ru
from loader import dp, db, bot

from utils.misc import show_on_gmaps

#Toshkent shahri tumanlari bo'yicha
@dp.message_handler(text="Весь город 🏙")
async def chilonzor(message: Message):
    stores = await db.select_storeByRegion_ru("Город Ташкент")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)

#Toshkent shahri tumanlari bo'yicha
@dp.message_handler(text="Мирзо-Улугбекский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Мирзо-Улугбекский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Мирабадский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Мирабадский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Шайхантахурский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Шайхантахурский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Алмазарский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Алмазарский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Юнусабадский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Юнусабадский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Сергелийский район")
async def sergeli(message: Message):
    stores = await db.select_store_ru("Сергелийский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("Наш магазин оптики не найден в вашем регионе")


@dp.message_handler(text="Яккасарайский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Яккасарайский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Чиланзарский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Чиланзарский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Учтепинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Учтепинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Бектемирский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Бектемирский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Яшнабадский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Яшнабадский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])

            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)

#Andijon viloyati tumanlari bo'yicha

@dp.message_handler(text="Шахриханский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Шахриханский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Ходжаабадский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ходжаабадский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Алтынкульский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Алтынкульский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Балыкчинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Балыкчинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Избосканский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Избосканский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Асакинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Асакинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Жалакудукский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Жалакудукский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Андижанский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Андижанский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Андижан")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Андижан")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


# Buxoro viloyati tumanlari bo'yicha

@dp.message_handler(text="Пешкунский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Пешкунский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Бухарский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Бухарский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Когонский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Когонский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Каракульский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Каракульский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Ромитанский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ромитанский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Бухара")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Бухара")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Бухара")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Бухара")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



# Farg'ona viloyati tumanlari bo'yicha

@dp.message_handler(text="Кувинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Кувинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Багдадский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Багдадский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Узбекистанский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Узбекистанский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Ферганский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ферганский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Язёванский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Язёванский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Риштанский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Риштанский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Учкуприкский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Учкуприкский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Дангаринский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Дангаринский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Фергана")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Фергана")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="город Коканд")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Коканд")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)

# Jizzax viloyati tumanlari bo'yicha

@dp.message_handler(text="город Джизак")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Джизак")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Фаришский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Фаришский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


# Jizzax viloyati tumanlari bo'yicha

@dp.message_handler(text="Чартакский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Чартакский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Косонсойский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Косонсойский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Учкурганский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Учкурганский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Янгикурганский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Янгикурганский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


#Navoiy viloyati tumanlari bo'yicha


@dp.message_handler(text="Кызылтепинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Кызылтепинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Карманинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Карманинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Навои")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Навои")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


# Qashqadaryo viloyati tumanlari bo'yicha


@dp.message_handler(text="город Карши")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Карши")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Косонский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Косонский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Чиракчинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Чиракчинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Шахрисабзский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Шахрисабзский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


#Qoraqalpog'iston respublikasi tumanlari bo'yicha

@dp.message_handler(text="город Нукус")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Нукус")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Турткульский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Турткульский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Кунградский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Кунградский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Ходжейлийский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ходжейлийский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


#Samarqand viloyati tumanlari bo'yicha

@dp.message_handler(text="Пайарыкский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Пайарыкский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Кошрабатский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Кошрабатский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Самаркандский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Самаркандский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Ургутский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ургутский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Самарканд")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Самарканд")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)

@dp.message_handler(text="Булунгурский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Булунгурский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


#Sirdaryo viloyati tumanlari bo'yicha

@dp.message_handler(text="Гулистанский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Гулистанский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Хавастский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Хавастский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Гулистан")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Гулистан")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Янгиерский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Янгиерский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Сырдарьинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Сырдарьинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


#Surxondaryo viloyati tumanlari bo'yicha magazinlar ro'yhati

@dp.message_handler(text="Джаркурганский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Джаркурганский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Узунский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Узунский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="город Термез")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Термез")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Шурчинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Шурчинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



#Toshkent viloyati tumanlari bo'yicha magazinlar ro'yhati

@dp.message_handler(text="Бекабадский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Бекабадский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Кибрайский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Кибрайский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Янгиюльский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Янгиюльский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Ташкентский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ташкентский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Бостанлыкский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Бостанлыкский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Зангиатинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Зангиатинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Паркентский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Паркентский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Алмалык")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Алмалык")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Ангрен")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Ангрен")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Бекабад")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Бекабад")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Чирчик")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Чирчик")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="город Нурафшан")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Нурафшан")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



#Xorazm viloyati tumanlari bo'yicha magazinlar ro'yhati
@dp.message_handler(text="Багатский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Багатский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Кошкупырский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Кошкупырский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Хазораспский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Хазораспский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Ханкинский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ханкинский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="Шовотский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Шовотский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Янгибазарский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Янгибазарский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



@dp.message_handler(text="город Ургенч")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("город Ургенч")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Гурланский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Гурланский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)


@dp.message_handler(text="Ургенчский район")
async def chilonzor(message: Message):
    stores = await db.select_store_ru("Ургенчский район")
    count = 0
    if stores:
        for store in stores:
            count = count+1
            locationUrl = show_on_gmaps.show(store['lat'], store['lang'])
            photo_url = store['store_photo']
            msg = f"👁 Optika: <b>{store['store_name_ru']}</b>\n\n"
            msg += f"📞 Telefon: {store['store_phone']}\n\n"
            msg += f"🏢 Shahar/viloyat: {store['region_ru']}\n\n"
            msg += f"📍 Manzil: {store['district_ru']}, {store['street_ru']}\n\n"
            msg += f"🏃 Lokatsiya: <a href='{locationUrl}'>{store['store_name_ru']}</a>\n"
            await message.answer_photo(photo_url, caption=msg)

        await message.answer(f"✅ Рядом найдено {count} магазина оптики.",reply_markup=BackButton_ru)
    else:
        await message.answer("❌ Магазин оптики не найден в вашем регионе",reply_markup=BackButton_ru)



