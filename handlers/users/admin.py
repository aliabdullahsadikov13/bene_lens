import asyncio

from aiogram import types
from asgiref.sync import async_to_sync

from data.config import ADMINS
from loader import dp, db, bot

@dp.message_handler(text="/allusers", user_id=ADMINS)
async def get_all_users(message: types.Message):

    users = await db.select_all_users()
    if users[0]:
        print(users[0])
    user_list = []
    for user in users:
        user_list.append(user[1])
        print(user[1])
    await message.answer(user_list)

@dp.message_handler(text="/reklama", user_id=ADMINS)
async def send_ad_to_all(message: types.Message):
    users = await db.select_all_users()
    print(users)
    for user in users:
        print(user[1])

        user_id = user[3]
        await bot.send_message(chat_id=user_id, text="@djangoDarslari kanaliga obuna bo'ling!")
        await asyncio.sleep(0.05)

@dp.message_handler(text="/cleandb", user_id=ADMINS)
async def get_all_users(message: types.Message):
    db.delete_users()
    await message.answer("Baza tozalandi!")


@dp.message_handler(text="/allstores", user_id=ADMINS)
async def get_all_stores(message: types.Message):

    stores = await db.select_all_stores()
    if stores[0]:
        print(stores[0])
    user_list = []
    for store in stores:
        user_list.append(store[1])
        print(store[1])
    await message.answer(user_list)



@dp.message_handler(text="/allinfo", user_id=ADMINS)
async def get_all_stores(message: types.Message):

    infos = await db.select_website_info_all()
    if infos[0]:
        print(infos[0])
    user_list = []
    for store in infos:
        user_list.append(store[1])
        print(store[1])
    await message.answer(user_list)