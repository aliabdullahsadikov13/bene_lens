import math

import asyncpg
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import Message

from keyboards.default.MenuKeyboard import RuMenu
from keyboards.default.OfflaynKeyboard import RuOfflayn
from keyboards.default.langChooseKeyboard import langMenu
from keyboards.default.locationKeyboard import location_keyboard_ru, location_Back_ru
from loader import dp, db
from states.locationStates import LocationData
from utils.misc import show_on_gmaps


def calc_distance(lat1, lon1, lat2, lon2):
    R = 6371000
    phi_1 = math.radians(lat1)
    phi_2 = math.radians(lat2)

    delta_phi = math.radians(lat2 - lat1)
    delta_lambda = math.radians(lon2 - lon1)

    a = math.sin(delta_phi / 2.0) ** 2 + \
        math.cos(phi_1) * math.cos(phi_2) * \
        math.sin(delta_lambda / 2.0) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    meters = R * c  # output distance in meters
    return meters / 1000.0  # output distance in kilometers

@dp.message_handler(text="📍 Отправить мое местоположение",state=None)
async def LocationSendRu(message: Message):
    await message.reply("Отправить местоположение",reply_markup=location_keyboard_ru)
    await LocationData.location_ru.set()

@dp.message_handler(content_types="location",state=LocationData.location_ru)
async def get_location(message:types.Message, state:FSMContext):
    location = message.location
    latitude = location.latitude
    longitude = location.longitude
    Shops = []
    stores = await db.select_all_stores()
    count = 0
    for store in stores:
        my_store = []
        store_location = {'lat': '', 'lon': ''}
        store_location['lat'] = float(store['lat'])
        store_location['lon'] = float(store['lang'])
        if store['store_name_uz']:
            store_name_uz = store['store_name_uz']
        if store['store_name_ru']:
            store_name_ru = store['store_name_ru']
        if store['store_phone']:
            store_phone = store['store_phone']
        if store['region_uz']:
            store_city_uz = store['region_uz']
        if store['region_ru']:
            store_city_ru = store['region_ru']
        if store['district_uz'] or store['street_uz']:
            store_address_uz = f"{store['district_uz']}, {store['street_uz']}"
        if store['district_ru'] or store['street_ru']:
            store_address_ru = f"{store['district_ru']}, {store['street_ru']}"
        if store['store_photo']:
            store_photo = store['store_photo']
        store_url = show_on_gmaps.show(**store_location)
        my_store.extend([store_name_uz, store_name_ru, store_phone, store_city_uz, store_city_ru, store_address_uz,
                         store_address_ru, store_photo,
                         calc_distance(latitude, longitude, store_location['lat'], store_location['lon']), store_url])
        my_store = tuple(my_store)
        Shops.append(my_store)

    closest_shops = sorted(Shops, key=lambda x: x[8], reverse=True)[-10:]

    await message.answer(f"<b>Проверьте список всех магазинов рядом с вами</b>")
    for store_name_uz, store_name_ru, shop_phone, shop_city_uz, shop_city_ru, shop_add_uz, shop_add_ru, \
        store_photo, distance, url in closest_shops:
        count = count + 1
        photo_url = store_photo
        msg = f"👁 Оптика: <b>{store_name_ru}</b> \n\n"
        msg += f"📞 Телефон: {shop_phone}\n\n"
        msg += f"🏢 Город/Область: {shop_city_ru}\n\n"
        msg += f"📍 Адрес: {shop_add_ru}\n\n"
        msg += f"🏃 Локация: <a href='{url}'>{store_name_ru}</a>\n\n"
        msg += f"🚗 Расстояние: {distance:.1f} km."
        await message.answer_photo(photo_url, caption=msg)
    await message.answer(f"✅ Рядом был обнаружен {count} магазин оптики.\n",reply_markup=location_Back_ru)



@dp.message_handler(text="◀️ Назад",state=LocationData.location_ru)
async def OflaynNazad(message: Message,state:FSMContext):
    await message.answer("▶️ Пожалуйста, выберите действие из меню",reply_markup=RuOfflayn)
    await state.reset_state()

@dp.message_handler(text="⏫  Главная",state=LocationData.location_ru)
async def LocationSendUz(message: Message,state:FSMContext):
    await message.answer("▶️ Пожалуйста, выберите действие из меню",reply_markup=RuMenu)
    await state.reset_state()

@dp.message_handler(commands='start',state=LocationData.location_ru)
async def bot_start(message: Message,state:FSMContext):
    try:
        user = await db.add_user(telegram_id=message.from_user.id,
                                 full_name=message.from_user.full_name,
                                 username=message.from_user.username)
    except asyncpg.exceptions.UniqueViolationError:
        user = await db.select_user(telegram_id=message.from_user.id)

    await message.answer(f"👋 Salom, {message.from_user.first_name}!\n"
                         f"🇺🇿 Iltimos, menyudan mavjud tilni tanlang.\n"
                         f"🇷🇺 Пожалуйста, выберите доступный язык из меню", reply_markup=langMenu)

    await state.reset_state()

@dp.message_handler(commands='help',state=LocationData.location_ru)
async def bot_help(message: types.Message,state:FSMContext):
    text = ("Buyruqlar: ",
            "/start - Запустить бота",
            "/help - Помощь")

    await message.answer("\n".join(text))
    await state.reset_state()