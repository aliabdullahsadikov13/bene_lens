import asyncpg
from aiogram.dispatcher.filters.builtin import CommandStart
from aiogram.types import Message, ReplyKeyboardRemove
from aiogram.types import InputFile

from keyboards.default.AddressOflayn import AddressOfflaynRu, AddressOfflaynUz, ToshkentUz, AndijonUz, BuxoroUz, \
    FargonaUz, JizzaxUz, NamanganUz, NavoiyUz, QashqadaryoUz, QoraqalpoqUz, SamarqandUz, SirdaryoUz, SurxondaryoUz, \
    ToshkentViloyatiUz, XorazmUz, ToshkentRu, AndijonRu, BuxoroRu, FargonaRu, XorazmRu, ToshkentViloyatiRu, \
    SurxondaryoRu, SirdaryoRu, SamarqandRu, QoraqalpoqRu, QashqadaryoRu, NavoiyRu, NamanganRu, JizzaxRu

from keyboards.default.MenuKeyboard import UzMenu,RuMenu
from keyboards.default.OfflaynKeyboard import UzOfflayn, RuOfflayn, OnlaynBackKeyboard_uz, OnlaynBackKeyboard_ru
from keyboards.default.langChooseKeyboard import langMenu
from keyboards.default.SettingsKeyboard import UzChangeLangPre, RuChangeLangPre
from keyboards.default.locationKeyboard import location_keyboard, location_keyboard_ru

from data.config import ADMINS

from loader import dp, db, bot


@dp.message_handler(CommandStart())
async def bot_start(message: Message):
    try:
        user = await db.add_user(telegram_id=message.from_user.id,
                                 full_name=message.from_user.full_name,
                                 username=message.from_user.username)
    except asyncpg.exceptions.UniqueViolationError:
        user = await db.select_user(telegram_id=message.from_user.id)

    await message.answer(f"👋 Salom, {message.from_user.first_name}!\n"
                         f"🇺🇿 Iltimos, menyudan mavjud tilni tanlang.\n"
                         f"🇷🇺 Пожалуйста, выберите доступный язык из меню", reply_markup=langMenu)

    # ADMINGA xabar beramiz
    # count = await db.count_users()
    # msg = f"{user[1]} bazaga qo'shildi.\nBazada {count} ta foydalanuvchi bor."
    # await bot.send_message(chat_id=ADMINS[0], text=msg)
    infos = await db.select_website_info_all()
    for info in infos:
        photo_url = f"media/{info['photoForCover']}"

        photo_url = InputFile(path_or_bytesio=photo_url)
    global img_url
    img_url = photo_url



@dp.message_handler(text="🇺🇿 O'zbek tili")
async def LangUz(message: Message):
    await message.answer("✅ O'zbek tili muvaffaqiyatli tanlandi.")
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzMenu)

@dp.message_handler(text='🇷🇺 Русский язык')
async def LangRu(message: Message):
    await message.answer("✅ Язык успешно выбран на Русский язык.", reply_markup=RuMenu)
    await message.answer("▶️Пожалуйста, выберите действие из меню.")

@dp.message_handler(text="⚙ Sozlamalar")
async def settingsUz(message: Message):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzChangeLangPre)

@dp.message_handler(text="🔄 Tilni oʻzgartirish")
async def settingsUz(message: Message):
    await message.answer("ℹ️Tilni oʻzgartirish uchun menyudan mavjud tilni tanlang.",reply_markup=langMenu)

@dp.message_handler(text="◀️Ortga")
async def BackUz(message: Message):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzMenu)


@dp.message_handler(text="⚙ Настройки")
async def settingsRu(message: Message):
    await message.answer("▶️Пожалуйста, выберите действие из меню.",reply_markup=RuChangeLangPre)

@dp.message_handler(text="🔄 Изменить язык")
async def settingsUz2(message: Message):
    await message.answer("ℹ️Чтобы изменить язык, выберите доступный язык из меню.",reply_markup=langMenu)

@dp.message_handler(text="◀️Назад")
async def settingsNazad(message: Message):
    await message.answer("▶️ Пожалуйста, выберите действие из меню.",reply_markup=RuMenu)


@dp.message_handler(text="🔎 Optika do'konlarini izlash")
async def OfflaynUz(message: Message):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzOfflayn)

@dp.message_handler(text="📍 Manzilimni yuborish")
async def LocationSendUz(message: Message):
    await message.reply("Lokatsiya yuboring",reply_markup=location_keyboard)

@dp.message_handler(text="🏠  Manzilni tanlash")
async def AddresChoice(message: Message):
    await message.answer("Quyidagi manzillarda bizning do'konlarimiz bor",reply_markup=AddressOfflaynUz)

@dp.message_handler(text="◀️Orgaga")
async def settingsUzNazad(message: Message):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzOfflayn)

@dp.message_handler(text="⏫  Bosh sahifa")
async def settingsUzNazad(message: Message):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzMenu)


@dp.message_handler(text="◀️ Ortga")
async def settingsUzNazad(message: Message):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=UzMenu)



@dp.message_handler(text="🇺🇿 O'zbekiston")
async def OflaynNazad(message: Message):
    await message.answer("Iltimos, menyudan hududingizni tanlang!",reply_markup=AddressOfflaynUz)


@dp.message_handler(text="🇺🇿 Узбекистан")
async def OflaynNazad(message: Message):
    await message.answer("Пожалуйста, выберите свой регион из меню!",reply_markup=AddressOfflaynRu)


# Toshkent shahri bosilsa, toshkent shahridagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Toshkent shahri")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:",reply_markup=ToshkentUz)

@dp.message_handler(text="⬅ Ortga")
async def settingsUzNazad(message: Message):
    await message.answer("▶️Iltimos, menyudan buyuruqni tanlang.",reply_markup=AddressOfflaynUz)

# Andijon viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Andijon viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:",reply_markup=AndijonUz)

# Buxoro viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Buxoro viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:",reply_markup=BuxoroUz)

# Farg'ona viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Farg`ona viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:",reply_markup=FargonaUz)

# Jizzax viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Jizzax viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=JizzaxUz)

# Namangan viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Namangan viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=NamanganUz)

# Navoiy viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Navoiy viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=NavoiyUz)

# Qashqadaryo viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Qashqadaryo viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=QashqadaryoUz)

# Qoraqalpog'iston Respublikasi bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Qoraqalpog'iston Respublikasi")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=QoraqalpoqUz)

# Samarqand viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Samarqand viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=SamarqandUz)

# Sirdaryo viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Sirdaryo viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=SirdaryoUz)

# Surxondaryo viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Surxondaryo viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=SurxondaryoUz)

# Toshkent viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Toshkent viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=ToshkentViloyatiUz)

# Xorazm viloyati bosilsa, Andijon viloyatidagi tumanlar ro'yhati chiqadi
@dp.message_handler(text="Xorazm viloyati")
async def OflaynNazad(message: Message):
    await message.answer("Tumanni tanlang:", reply_markup=XorazmUz)


@dp.message_handler(text="🔎 Искать в магазинах оптики")
async def OfflaynRu(message: Message):
    await message.answer("▶️ Пожалуйста, выберите действие из меню",reply_markup=RuOfflayn)

@dp.message_handler(text="🏠  Выбор места")
async def OflaynNazad(message: Message):
    await message.answer("У нас есть магазины по следующим адресам",reply_markup=AddressOfflaynRu)


@dp.message_handler(text="⬅ Назад")
async def LocationSendUz(message: Message):
    await message.answer("▶️ Пожалуйста, выберите действие из меню",reply_markup=AddressOfflaynRu)

@dp.message_handler(text="⏫  Главная")
async def LocationSendUz(message: Message):
    await message.answer("▶️ Пожалуйста, выберите действие из меню",reply_markup=RuMenu)


@dp.message_handler(text="📍 Отправить мое местоположение")
async def LocationSendRu(message: Message):
    await message.reply("Отправить местоположение",reply_markup=location_keyboard_ru)


@dp.message_handler(text="◀️ Назад")
async def OflaynNazad(message: Message):
    await message.answer("▶️ Пожалуйста, выберите действие из меню",reply_markup=RuOfflayn)

# появится список районов Ташкента.
@dp.message_handler(text="Город Ташкент")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:",reply_markup=ToshkentRu)

# появится список районов Андижанская область.
@dp.message_handler(text="Андижанская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:",reply_markup=AndijonRu)

# появится список районов Бухарская область.
@dp.message_handler(text="Бухарская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:",reply_markup=BuxoroRu)

# появится список районов Ферганской области.
@dp.message_handler(text="Ферганская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:",reply_markup=FargonaRu)

# появится список районов Джизакская область.
@dp.message_handler(text="Джизакская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=JizzaxRu)

# появится список районов Наманганская область.
@dp.message_handler(text="Наманганская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=NamanganRu)

# появится список районов Навоийская область.
@dp.message_handler(text="Навоийская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=NavoiyRu)

# появится список районов Кашкадарьинская область
@dp.message_handler(text="Кашкадарьинская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=QashqadaryoRu)
    rayon = message.text
    print(rayon)


# появится список районов Республика Каракалпакстан.
@dp.message_handler(text="Республика Каракалпакстан")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=QoraqalpoqRu)

# появится список районов Самаркандская область.
@dp.message_handler(text="Самаркандская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=SamarqandRu)

# появится список районов Сырдарьинская область.
@dp.message_handler(text="Сырдарьинская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=SirdaryoRu)

# появится список районов Сурхандарьинская область.
@dp.message_handler(text="Сурхандарьинская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=SurxondaryoRu)

# появится список районов Ташкентская область.
@dp.message_handler(text="Ташкентская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=ToshkentViloyatiRu)

# появится список районов Хорезмская область.
@dp.message_handler(text="Хорезмская область")
async def OflaynNazad(message: Message):
    await message.answer("Выберите район:", reply_markup=XorazmRu)


#onlayn sotib olish qismi rus tilida
@dp.message_handler(text="🌐 Поиск в интернет-магазинах оптики")
async def KupitOnlayn(message: Message):
    # await message.answer("Пожалуйста подождите...")
    infos = await db.select_website_info_all()
    for info in infos:
        msg = "<b>Наши контакты:</b>\n\n"
        if info['website']:
            msg += f"<b>Сайт:</b> {info['website']}  \n\n"
        if info['phoneNumber']:
            msg += f"<b>Телефон</b>: {info['phoneNumber']}\n\n"
        if info['shortInfoRu']:
            msg += f"{info['shortInfoRu']}\n\n"
        if info['facebook']:
            msg += f"<a href='{info['facebook']}'><b>Facebook | </b></a>"
        if info['instagram']:
            msg += f"<a href='{info['instagram']}'><b>Instagram | </b></a>"
        if info['telegram']:
            msg += f"<a href='{info['telegram']}'><b>Telegram</b></a>\n\n"

        # photo_url = 'https://media.istockphoto.com/photos/closeup-image-of-male-hands-using-smartphone-with-icon-telephone-picture-id1168945108?k=20&m=1168945108&s=612x612&w=0&h=MqKJyjMB1NJ33aRB9kjhxqP_GbPmEVB11saJi9sCuwM='
        # photo_url = f"djangoBeneLens/media/{info['photoForCover']}"
        #
        # photo_url = InputFile(path_or_bytesio=photo_url)

    await message.answer(msg, disable_web_page_preview=True,reply_markup=OnlaynBackKeyboard_ru)

#onlayn sotib olish qismi o'zbek tilida
@dp.message_handler(text="🌐 Internet optika do'konlarini izlash")
async def OnlaynSotibOlish(message: Message):
    await message.answer("Iltimos, biroz kutib turing...")
    infos = await db.select_website_info_all()
    for info in infos:
        msg = "<b>Aloqa ma'lumotlarimiz:</b>\n\n"
        if info['website']:
            msg += f"<b>Veb-sayt:</b> {info['website']}\n\n"
        if info['phoneNumber']:
            msg += f"<b>Telefon:</b> {info['phoneNumber']}\n\n"
        if info['shortInfoUz']:
            msg += f"<b>{info['shortInfoUz']}\n\n</b>"
        if info['facebook']:
            msg += f"<a href='{info['facebook']}'>Facebook | </a>"
        if info['instagram']:
            msg += f"<a href='{info['instagram']}'>Instagram | </a>"
        if info['telegram']:
            msg += f"<a href='{info['telegram']}'>Telegram</a>\n\n"

        # photo_url = f"djangoBeneLens/media/{info['photoForCover']}"
        #
        # photo_url = InputFile(path_or_bytesio=photo_url)
        # print(photo_url)
        # photo_url = 'https://media.istockphoto.com/photos/closeup-image-of-male-hands-using-smartphone-with-icon-telephone-picture-id1168945108?k=20&m=1168945108&s=612x612&w=0&h=MqKJyjMB1NJ33aRB9kjhxqP_GbPmEVB11saJi9sCuwM='

        await message.answer(msg, disable_web_page_preview=True,reply_markup=OnlaynBackKeyboard_uz)


@dp.message_handler(is_forwarded=True)
async def get_location(message: Message):

    await message.answer("Iltimos forward xabarlarni yubormang")
