from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

location_keyboard = ReplyKeyboardMarkup(resize_keyboard=True,
                               keyboard=[
                                   [
                                       KeyboardButton(text="📍 Manzilimni yubor",
                                                      request_location=True)
                                   ],
                                    [
                                        KeyboardButton(text="◀️Orgaga"),
                                        KeyboardButton(text="⏫  Bosh sahifa"),
                                   ],
                               ])

location_Back_uz = ReplyKeyboardMarkup(resize_keyboard=True,
                               keyboard=[

                                    [
                                        KeyboardButton(text="◀️Orgaga"),
                                        KeyboardButton(text="⏫  Bosh sahifa"),
                                   ],
                               ])


location_keyboard_ru = ReplyKeyboardMarkup(resize_keyboard=True,
                               keyboard=[
                                   [
                                       KeyboardButton(text="📍 Отправить мое местоположение",
                                                      request_location=True)
                                   ],
                                    [
                                        KeyboardButton(text="◀️ Назад"),
                                        KeyboardButton(text="⏫  Главная"),
                                   ],
                               ])

location_Back_ru = ReplyKeyboardMarkup(resize_keyboard=True,
                               keyboard=[

                                    [
                                        KeyboardButton(text="◀️ Назад"),
                                        KeyboardButton(text="⏫  Главная"),
                                   ],
                               ])