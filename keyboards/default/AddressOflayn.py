from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

AddressOfflaynUz = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🇺🇿 O'zbekiston"),
        ],
        [
            KeyboardButton(text="Toshkent shahri"),
        ],
        [
            KeyboardButton(text="Andijon viloyati"),
            KeyboardButton(text="Buxoro viloyati"),
        ],
        [
            KeyboardButton(text="Farg`ona viloyati"),
            KeyboardButton(text="Jizzax viloyati"),
        ],
        [
            KeyboardButton(text="Namangan viloyati"),
            KeyboardButton(text="Navoiy viloyati"),
        ],
        [
            KeyboardButton(text="Qashqadaryo viloyati"),
            KeyboardButton(text="Qoraqalpog`iston Respublikasi"),
        ],
        [
            KeyboardButton(text="Samarqand viloyati"),
            KeyboardButton(text="Sirdaryo viloyati"),
        ],
        [
            KeyboardButton(text="Surxondaryo viloyati"),
            KeyboardButton(text="Toshkent viloyati"),
        ],
        [
            KeyboardButton(text="Xorazm viloyati"),
        ],
        [
            KeyboardButton(text="◀️Orgaga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],
    ],
    resize_keyboard=True
)

#toshkent shahri tumanlari ro`yhati klavishlar bilan birgalikda
ToshkentUz = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="Butun shahar 🏙"),
        ],
        [
            KeyboardButton(text="Mirzo Ulug`bek tumani"),
            KeyboardButton(text="Mirobod tumani"),
        ],
        [
            KeyboardButton(text="Shayxontohur tumani"),
            KeyboardButton(text="Olmazor tumani"),
        ],
        [
            KeyboardButton(text="Yunusobod tumani"),
            KeyboardButton(text="Sergeli tumani"),
        ],
        [
            KeyboardButton(text="Yakkasaroy tumani"),
            KeyboardButton(text="Chilonzor tumani"),
        ],
        [
            KeyboardButton(text="Uchtepa tumani"),
            KeyboardButton(text="Bektemir tumani"),
        ],
        [
            KeyboardButton(text="Yashnobod tumani"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)


#Andijon viloyati tumanlari ro`yhati klavishlar bilan birgalikda
AndijonUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Shaxrixon tumani"),
            KeyboardButton(text="Xo`jaobod tumani"),
        ],
        [
            KeyboardButton(text="Oltinko`l tumani"),
            KeyboardButton(text="Baliqchi tumani"),
        ],
        [
            KeyboardButton(text="Izboskan tumani"),
            KeyboardButton(text="Asaka tumani"),
        ],
        [
            KeyboardButton(text="Jalaquduq tumani"),
            KeyboardButton(text="Andijon tumani"),
        ],
        [
            KeyboardButton(text="Andijon shahri"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Buxoro viloyati tumanlari ro`yhati klavishlar bilan birgalikda
BuxoroUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Peshku tumani"),
            KeyboardButton(text="Buxoro tumani"),
        ],
        [
            KeyboardButton(text="Kogon tumani"),
            KeyboardButton(text="Qorako`l tumani"),
        ],
        [
            KeyboardButton(text="Romitan tumani"),
            KeyboardButton(text="Buxoro shahri"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)


#Farg`ona viloyati tumanlari ro`yhati klavishlar bilan birgalikda
FargonaUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Quva tumani"),
            KeyboardButton(text="Bog`dod tumani"),
        ],
        [
            KeyboardButton(text="O'zbekiston tumani"),
            KeyboardButton(text="Farg`ona tumani"),
        ],
        [
            KeyboardButton(text="Yozyovon tumani"),
            KeyboardButton(text="Rishton tumani"),
        ],
        [
            KeyboardButton(text="Uchko`prik tumani"),
            KeyboardButton(text="Dang`ara tumani"),
        ],
        [
            KeyboardButton(text="Farg`ona shahri"),
            KeyboardButton(text="Qo`qon shahri"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Jizzax viloyati tumanlari ro`yhati klavishlar bilan birgalikda
JizzaxUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Jizzax shahri"),
            KeyboardButton(text="Forish tumani"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Namangan viloyati tumanlari ro`yhati klavishlar bilan birgalikda
NamanganUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Chortoq tumani"),
            KeyboardButton(text="Kosonsoy tumani"),
        ],
        [
            KeyboardButton(text="Uchqo`rg`on tumani"),
            KeyboardButton(text="Yangiqo`rg`on tumani"),
        ],

        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Navoiy viloyati tumanlari ro`yhati klavishlar bilan birgalikda
NavoiyUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Qiziltepa tumani"),
            KeyboardButton(text="Karmana tumani"),
        ],
        [
            KeyboardButton(text="Navoiy shahri"),
        ],

        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Qashqadaryo viloyati tumanlari ro`yhati klavishlar bilan birgalikda
QashqadaryoUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Qarshi shahri"),
            KeyboardButton(text="Koson tumani"),
        ],
        [
            KeyboardButton(text="Chiroqchi tumani"),
            KeyboardButton(text="Shahrisabz tumani"),
        ],

        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)


#Qoraqalpog`iston Respublikasi viloyati tumanlari ro`yhati klavishlar bilan birgalikda
QoraqalpoqUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Nukus shahri"),
            KeyboardButton(text="To`rtko`l tumani"),
        ],
        [
            KeyboardButton(text="Qo`ng`irot tumani"),
            KeyboardButton(text="Xo`jayli tumani"),
        ],

        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Samarqand viloyati tumanlari ro`yhati klavishlar bilan birgalikda
SamarqandUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Payariq tumani"),
            KeyboardButton(text="Qo`shrabot tumani"),
        ],
        [
            KeyboardButton(text="Samarqand tumani"),
            KeyboardButton(text="Urgut tumani"),
        ],
        [
            KeyboardButton(text="Samarqand shahri"),
            KeyboardButton(text="Bulung`ur tumani"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Sirdaryo viloyati tumanlari ro`yhati klavishlar bilan birgalikda
SirdaryoUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Guliston tumani"),
            KeyboardButton(text="Xovos tumani"),
        ],
        [
            KeyboardButton(text="Guliston shahri"),
            KeyboardButton(text="Yangiyer tumani"),
        ],
        [
            KeyboardButton(text="Sirdaryo tumani"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Surxondaryo viloyati tumanlari ro`yhati klavishlar bilan birgalikda
SurxondaryoUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Jarqo`rg`on tumani"),
            KeyboardButton(text="Uzun tumani"),
        ],
        [
            KeyboardButton(text="Termiz shahri"),
            KeyboardButton(text="Sho`rchi tumani"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Toshkent viloyati tumanlari ro`yhati klavishlar bilan birgalikda
ToshkentViloyatiUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Bekobod tumani"),
            KeyboardButton(text="Qibray tumani"),
        ],
        [
            KeyboardButton(text="Yangiyo`l tumani"),
            KeyboardButton(text="Toshkent tumani"),
        ],
        [
            KeyboardButton(text="Bo`stonliq tumani"),
            KeyboardButton(text="Zangiota tumani"),
        ],
        [
            KeyboardButton(text="Parkent tumani"),
            KeyboardButton(text="Olmaliq shahri"),
        ],
        [
            KeyboardButton(text="Angren shahri"),
            KeyboardButton(text="Bekobod shahri"),
        ],
        [
            KeyboardButton(text="Chirchiq shahri"),
            KeyboardButton(text="Nurafshon shahri"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)

#Xorazm viloyati tumanlari ro`yhati klavishlar bilan birgalikda
XorazmUz = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Bog`ot tumani"),
            KeyboardButton(text="Qo`shko`pir tumani"),
        ],
        [
            KeyboardButton(text="Xazorasp tumani"),
            KeyboardButton(text="Xonqa tumani"),
        ],
        [
            KeyboardButton(text="Shovot tumani"),
            KeyboardButton(text="Yangibozor tumani"),
        ],
        [
            KeyboardButton(text="Urganch shahri"),
            KeyboardButton(text="Gurlan tumani"),
        ],
        [
            KeyboardButton(text="Urganch tumani"),
        ],
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)


AddressOfflaynRu = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🇺🇿 Узбекистан"),
        ],
        [
            KeyboardButton(text="Город Ташкент"),
        ],
        [
            KeyboardButton(text="Андижанская область"),
            KeyboardButton(text="Бухарская область"),
        ],
        [
            KeyboardButton(text="Ферганская область"),
            KeyboardButton(text="Джизакская область"),
        ],
        [
            KeyboardButton(text="Наманганская область"),
            KeyboardButton(text="Навоийская область"),
        ],
        [
            KeyboardButton(text="Кашкадарьинская область"),
            KeyboardButton(text="Республика Каракалпакстан"),
        ],
        [
            KeyboardButton(text="Самаркандская область"),
            KeyboardButton(text="Сырдарьинская область"),
        ],
        [
            KeyboardButton(text="Сурхандарьинская область"),
            KeyboardButton(text="Ташкентская область"),
        ],
        [
            KeyboardButton(text="Хорезмская область"),
        ],
        [
            KeyboardButton(text="◀️ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],
    ],
    resize_keyboard=True
)

#Список районов города Ташкента
ToshkentRu = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="Весь город 🏙"),
        ],
        [
            KeyboardButton(text="Мирзо-Улугбекский район"),
            KeyboardButton(text="Мирабадский район"),
        ],
        [
            KeyboardButton(text="Шайхантахурский район"),
            KeyboardButton(text="Алмазарский район"),
        ],
        [
            KeyboardButton(text="Юнусабадский район"),
            KeyboardButton(text="Сергелийский район"),
        ],
        [
            KeyboardButton(text="Яккасарайский район"),
            KeyboardButton(text="Чиланзарский район"),
        ],
        [
            KeyboardButton(text="Учтепинский район"),
            KeyboardButton(text="Бектемирский район"),
        ],
        [
            KeyboardButton(text="Яшнабадский район"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Список районов Андижанской области с ключами
AndijonRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Шахриханский район"),
            KeyboardButton(text="Ходжаабадский район"),
        ],
        [
            KeyboardButton(text="Алтынкульский район"),
            KeyboardButton(text="Балыкчинский район"),
        ],
        [
            KeyboardButton(text="Избосканский район"),
            KeyboardButton(text="Асакинский район"),
        ],
        [
            KeyboardButton(text="Жалакудукский район"),
            KeyboardButton(text="Андижанский район"),
        ],
        [
            KeyboardButton(text="город Андижан"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

# Список районов Бухарской области с ключами
BuxoroRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Пешкунский район"),
            KeyboardButton(text="Бухарский район"),
        ],
        [
            KeyboardButton(text="Когонский район"),
            KeyboardButton(text="Каракульский район"),
        ],
        [
            KeyboardButton(text="Ромитанский район"),
            KeyboardButton(text="город Бухара"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)


# Список районов Ферганской области с ключами
FargonaRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Кувинский район"),
            KeyboardButton(text="Багдадский район"),
        ],
        [
            KeyboardButton(text="Узбекистанский район"),
            KeyboardButton(text="Ферганский район"),
        ],
        [
            KeyboardButton(text="Язёванский район"),
            KeyboardButton(text="Риштанский район"),
        ],
        [
            KeyboardButton(text="Учкуприкский район"),
            KeyboardButton(text="Дангаринский район"),
        ],
        [
            KeyboardButton(text="город Фергана"),
            KeyboardButton(text="город Коканд "),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Jizzax viloyati tumanlari ro`yhati klavishlar bilan birgalikda
JizzaxRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="город Джизак"),
            KeyboardButton(text="Фаришский район"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Namangan viloyati tumanlari ro`yhati klavishlar bilan birgalikda
NamanganRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Чартакский район"),
            KeyboardButton(text="Косонсойский район"),
        ],
        [
            KeyboardButton(text="Учкурганский район"),
            KeyboardButton(text="Янгикурганский район"),
        ],

        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Navoiy viloyati tumanlari ro`yhati klavishlar bilan birgalikda
NavoiyRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Кызылтепинский район"),
            KeyboardButton(text="Карманинский район"),
        ],
        [
            KeyboardButton(text="город Навои"),
        ],

        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Qashqadaryo viloyati tumanlari ro`yhati klavishlar bilan birgalikda
QashqadaryoRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="город Карши"),
            KeyboardButton(text="Косонский район"),
        ],
        [
            KeyboardButton(text="Чиракчинский район"),
            KeyboardButton(text="Шахрисабзский район"),
        ],

        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)


#Qoraqalpog`iston Respublikasi viloyati tumanlari ro`yhati klavishlar bilan birgalikda
QoraqalpoqRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="город Нукус"),
            KeyboardButton(text="Турткульский район"),
        ],
        [
            KeyboardButton(text="Кунградский район"),
            KeyboardButton(text="Ходжейлийский район"),
        ],

        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Samarqand viloyati tumanlari ro`yhati klavishlar bilan birgalikda
SamarqandRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Пайарыкский район"),
            KeyboardButton(text="Кошрабатский район"),
        ],
        [
            KeyboardButton(text="Самаркандский район"),
            KeyboardButton(text="Ургутский район"),
        ],
        [
            KeyboardButton(text="город Самарканд"),
            KeyboardButton(text="Булунгурский район"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Sirdaryo viloyati tumanlari ro`yhati klavishlar bilan birgalikda
SirdaryoRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Гулистанский район"),
            KeyboardButton(text="Хавастский район"),
        ],
        [
            KeyboardButton(text="город Гулистан"),
            KeyboardButton(text="Янгиерский район"),
        ],
        [
            KeyboardButton(text="Сырдарьинский район"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Surxondaryo viloyati tumanlari ro`yhati klavishlar bilan birgalikda
SurxondaryoRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Джаркурганский район"),
            KeyboardButton(text="Узунский район"),
        ],
        [
            KeyboardButton(text="город Термез"),
            KeyboardButton(text="Шурчинский район"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Toshkent viloyati tumanlari ro`yhati klavishlar bilan birgalikda
ToshkentViloyatiRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Бекабадский район"),
            KeyboardButton(text="Кибрайский район"),
        ],
        [
            KeyboardButton(text="Янгиюльский район"),
            KeyboardButton(text="Ташкентский район"),
        ],
        [
            KeyboardButton(text="Бостанлыкский район"),
            KeyboardButton(text="Зангиатинский район"),
        ],
        [
            KeyboardButton(text="Паркентский район"),
            KeyboardButton(text="город Алмалык"),
        ],
        [
            KeyboardButton(text="город Ангрен"),
            KeyboardButton(text="город Бекабад"),
        ],
        [
            KeyboardButton(text="город Чирчик"),
            KeyboardButton(text="город Нурафшан"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)

#Xorazm viloyati tumanlari ro`yhati klavishlar bilan birgalikda
XorazmRu = ReplyKeyboardMarkup(
    keyboard = [

        [
            KeyboardButton(text="Багатский район"),
            KeyboardButton(text="Кошкупырский район"),
        ],
        [
            KeyboardButton(text="Хазораспский район"),
            KeyboardButton(text="Ханкинский район"),
        ],
        [
            KeyboardButton(text="Шовотский район"),
            KeyboardButton(text="Янгибазарский район"),
        ],
        [
            KeyboardButton(text="город Ургенч"),
            KeyboardButton(text="Гурланский район"),
        ],
        [
            KeyboardButton(text="Ургенчский район"),
        ],
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)


BackButton_uz = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="⬅ Ortga"),
            KeyboardButton(text="⏫  Bosh sahifa"),
        ],

    ],
    resize_keyboard=True
)


BackButton_ru = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="⬅ Назад"),
            KeyboardButton(text="⏫  Главная"),
        ],

    ],
    resize_keyboard=True
)
