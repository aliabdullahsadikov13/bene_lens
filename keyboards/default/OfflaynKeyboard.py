from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

UzOfflayn = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🏠  Manzilni tanlash"),
        ],
        [
            KeyboardButton(text="📍 Manzilimni yuborish"),
        ],
        [
            KeyboardButton(text="◀️ Ortga"),
        ]
    ],
    resize_keyboard=True
)

RuOfflayn = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🏠  Выбор места"),
        ],
        [
            KeyboardButton(text="📍 Отправить мое местоположение"),
        ],
        [
            KeyboardButton(text="◀️Назад"),
        ]
    ],
    resize_keyboard=True
)

OnlaynBackKeyboard_ru = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="◀️Назад"),
        ]
    ],
    resize_keyboard=True
)

OnlaynBackKeyboard_uz = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="◀️ Ortga"),
        ]
    ],
    resize_keyboard=True
)
