from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

UzChangeLangPre = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🔄 Tilni oʻzgartirish"),
        ],
        [
            KeyboardButton(text="◀️Ortga"),
        ]
    ],
    resize_keyboard=True
)

RuChangeLangPre = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🔄 Изменить язык"),
        ],
        [
            KeyboardButton(text="◀️Назад"),
        ]
    ],
    resize_keyboard=True
)