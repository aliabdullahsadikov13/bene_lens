from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

langMenu = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🇺🇿 O'zbek tili"),
            KeyboardButton(text='🇷🇺 Русский язык'),
        ],
    ],
    resize_keyboard=True
)