from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

UzMenu = ReplyKeyboardMarkup(
    keyboard = [
        [
            KeyboardButton(text="🔎 Optika do'konlarini izlash"),

        ],
        [
            KeyboardButton(text="🌐 Internet optika do'konlarini izlash"),
        ],
        [
            KeyboardButton(text="⚙ Sozlamalar"),
        ],

    ],
    resize_keyboard=True
)

RuMenu = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="🔎 Искать в магазинах оптики"),

        ],
        [
            KeyboardButton(text="🌐 Поиск в интернет-магазинах оптики"),
        ],
        [
            KeyboardButton(text="⚙ Настройки"),
        ],

    ],
    resize_keyboard=True
)