from django.contrib import admin
from .models import User, Product, Category, Store, Contact, \
    WebInfoModel, Region, District, ChangineTimeLenses, Brand, good_with, ClientOnlineStores
from modeltranslation.admin import TranslationAdmin


class RegionAdmin(TranslationAdmin):
    pass


class DistrictAdmin(TranslationAdmin):
    pass


class StoreAdmin(TranslationAdmin):
    pass


class CategoryAdmin(TranslationAdmin):
    pass


class ProductAdmin(TranslationAdmin):
    pass



class ChanginTimeAdmin(TranslationAdmin):
    pass



class BrandAdmin(TranslationAdmin):
    pass



class GoodWithAdmin(TranslationAdmin):
    pass


admin.site.register(User)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(District, DistrictAdmin)
admin.site.register(Store, StoreAdmin)
admin.site.register(Contact)
admin.site.register(WebInfoModel)
admin.site.register(ChangineTimeLenses, ChanginTimeAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(good_with, GoodWithAdmin)
admin.site.register(ClientOnlineStores)
