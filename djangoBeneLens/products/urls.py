from django.urls import path

from .views import DashHomeView, DashAllCategoryView, DashAddCategoryView, DashCategoryDeleteView, \
    DashCategoryEditView, ProductsAllView, ProductAddView, ProductDetailView, ProductEditView, \
    ProductDeleteView, DashStoreAllView, DashStoreDetailView, DashStoreEditView, DashStoreDeleteView, \
    DashStoreAddView, ContactFormAllView, ContactDeleteView, OnlaynMagazinAll, OnlaynMagazinUpdateView, \
    OnlaynMagazinCreateView, OnlaynMagazinDeleteView, load_districts_ru, fill_region_uz, fill_district_uz, \
    DashChangeTimeAll, DashAddChangeTimeView, DashChangeTimeDeleteView, DashChangeTimeEditView, \
    DashBrandAll, DashBrandAdd, DashBrandDelete, DashBrandEdit, Good_with_list, Good_with_add_view, \
    Good_with_delete_view, Good_with_edit_view, ReviewListView, ReviewAddView, ReviewEditView, ReviewDeleteView,\
    StoreOnlineAllView, StoreOnlineUpdateView, StoreOnlineCreateView, StoreOnlineDeleteView

urlpatterns = [
    path('', DashHomeView.as_view(), name='dashboard'),
    path('category/all', DashAllCategoryView.as_view(), name='dashboardAllCategory'),
    path('category/add', DashAddCategoryView.as_view(), name='dashboardAddCategory'),
    path('category/edit/<int:pk>', DashCategoryEditView.as_view(), name="dashboardCategoryEditView"),
    path('category/delete/<int:pk>', DashCategoryDeleteView.as_view(), name="dashboardCategoryDeleteView"),
    path('brand/all', DashBrandAll.as_view(), name='dash_brand_all'),
    path('brand/add', DashBrandAdd.as_view(), name='dash_brand_add'),
    path('brand/edit/<int:pk>', DashBrandEdit.as_view(), name="dash_brand_edit"),
    path('brand/delete/<int:pk>', DashBrandDelete.as_view(), name="dash_brand_delete"),
    path('review/all', ReviewListView.as_view(), name='dash_review_all'),
    path('review/add', ReviewAddView.as_view(), name='dash_review_add'),
    path('review/edit/<int:pk>', ReviewEditView.as_view(), name="dash_review_edit"),
    path('review/delete/<int:pk>', ReviewDeleteView.as_view(), name="dash_review_delete"),
    path('goodwith/all', Good_with_list.as_view(), name='good_with_list'),
    path('goodwith/add', Good_with_add_view.as_view(), name='good_with_add'),
    path('goodwith/edit/<int:pk>', Good_with_edit_view.as_view(), name="good_with_edit"),
    path('goodwith/delete/<int:pk>', Good_with_delete_view.as_view(), name="good_with_delete"),
    path('changingtime/', DashChangeTimeAll.as_view(), name='ChangtingTime'),
    path('changingtime/add', DashAddChangeTimeView.as_view(), name='changing_time_add'),
    path('changingtime/delete/<int:pk>', DashChangeTimeDeleteView.as_view(), name="Changing_time_delete"),
    path('changingtime/edit/<int:pk>', DashChangeTimeEditView.as_view(), name="Changing_time_edit"),
    path('product/all', ProductsAllView.as_view(), name='productList'),
    path('product/add', ProductAddView.as_view(), name='productAdd'),
    path('product/<int:pk>', ProductDetailView.as_view(), name='productDetail'),
    path('product/<int:pk>/edit', ProductEditView.as_view(), name='productedit'),
    path('product/<int:pk>/delete', ProductDeleteView.as_view(), name='productdelete'),
    path('store/all', DashStoreAllView.as_view(), name='dashStore_all'),
    path('store/<int:pk>', DashStoreDetailView.as_view(), name='dashStore_detail'),
    path('store/add', DashStoreAddView.as_view(), name='dashStore_add'),
    path('store/<int:pk>/edit', DashStoreEditView.as_view(), name='dashStore_edit'),
    path('store/<int:pk>/delete', DashStoreDeleteView.as_view(), name='dashStore_delete'),
    path('messages/all', ContactFormAllView.as_view(), name='all_messages'),
    path('messages/delete/<int:pk>', ContactDeleteView.as_view(), name="messageDelete"),
    path('onlinemagazin/all', OnlaynMagazinAll.as_view(), name='all_online_magazines'),
    path('onlinemagazin/add', OnlaynMagazinCreateView.as_view(), name='online_magazine_create'),
    path('onlinemagazin/edit/<int:pk>', OnlaynMagazinUpdateView.as_view(), name="online_magazine_update"),
    path('onlinemagazin/delete/<int:pk>', OnlaynMagazinDeleteView.as_view(), name="dash_brand_delete"),
    path('store-online/all', StoreOnlineAllView.as_view(), name='all_store_online'),
    path('store-online/add', StoreOnlineCreateView.as_view(), name='add_store_online'),
    path('store-onlin/edit/<int:pk>', StoreOnlineUpdateView.as_view(), name="update_store_online"),
    path('store-online/delete/<int:pk>', StoreOnlineDeleteView.as_view(), name="delete_store_online"),
    path('ajax/load-districts-ru/', load_districts_ru, name='ajax_load_districts_ru'),
    path('ajax/fill-region-uz/', fill_region_uz, name='fill_region_uz'),
    path('ajax/fill-district-uz/', fill_district_uz, name='fill_district_uz'),
]
