from django.core.validators import RegexValidator
from django.db import models


# Create your models here.
from django.urls import reverse


class User(models.Model):
    id = models.AutoField(primary_key=True)
    full_name = models.CharField(verbose_name="Ism", max_length=200)
    username = models.CharField(verbose_name="Telegram username", max_length=200, null=True)
    telegram_id = models.BigIntegerField(verbose_name="Telegram ID", unique=True, default=1)

    def __str__(self):
        return f"{self.id} - {self.telegram_id} - {self.full_name}"


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    categoryName = models.CharField(verbose_name="Название категории", max_length=200)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self):
        return self.categoryName


class Brand(models.Model):
    id = models.AutoField(primary_key=True)
    brand_name = models.CharField(max_length=100)
    brand_text = models.CharField(max_length=200, null=True, blank=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.brand_name


class ChangineTimeLenses(models.Model):
    id = models.AutoField(primary_key=True)
    time = models.CharField(max_length=150)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Changing time"

    def __str__(self):
        return self.time


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE, verbose_name="Категория продукта"
    )

    productname = models.CharField(verbose_name="Название продукта", max_length=50)
    brand_name = models.ForeignKey(
        Brand,
        on_delete=models.CASCADE,
        verbose_name="Бранд продукта",
        null=True, blank=True
    )
    sezon = models.CharField(verbose_name=".... вақти", max_length=100, null=True)
    description = models.TextField(verbose_name="Описание продукта", max_length=3000, null=True)
    features = models.CharField(verbose_name="доступные возможности", max_length=200, null=True)
    productPhoto = models.ImageField(verbose_name="Фото продукте", upload_to='products/')
    photo_upakovka = models.ImageField(verbose_name="Фото упаковка продукте", upload_to='products/', null=True)
    lensePhoto = models.ImageField(verbose_name="Фото линза", upload_to='products/')
    price = models.DecimalField(verbose_name="Цена", decimal_places=2, max_digits=8)
    ageGroup = models.CharField(verbose_name="С какого возраста", max_length=50)
    time_to_change = models.ForeignKey(
        ChangineTimeLenses,
        on_delete=models.CASCADE,
        verbose_name="Срок замены линза")
    diameter = models.CharField(verbose_name="Диаметр линзы", max_length=10, null=True)
    radius = models.CharField(verbose_name='Радиус кривизны линзы', max_length=20, null=True)
    humidity = models.CharField(verbose_name="Влажность", max_length=10, null=True)
    material_type = models.CharField(verbose_name="Тип материала", max_length=100, null=True)
    design = models.CharField(verbose_name="Дизайн линзы", max_length=100, null=True)
    time_of_use = models.CharField(verbose_name="Время использования", max_length=100, null=True)
    number_of_blisters = models.CharField(verbose_name="Количество блистеров в упаковке", max_length=100, null=True)
    country = models.CharField(verbose_name="Страна производства", max_length=100, null=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"№{self.id} {self.category} - {self.productname} - {self.price}"

    def get_absolute_url(self):  # new
        return reverse('productDetail', args=[str(self.id)])


class good_with(models.Model):
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        verbose_name="продукт",
    )
    name = models.CharField(max_length=150, verbose_name="ХОРОШО СОЧЕТАЕТСЯ С")

    def __str__(self):
        return self.name


class Region(models.Model):   # Region = Country
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class District(models.Model):    # District = City
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Store(models.Model):
    store_name = models.CharField(verbose_name="Название магазина", max_length=200)
    store_photo = models.CharField(verbose_name='Ссылка на фото магазина',
                                   max_length=300, default="https://ibb.co/jLvD982")
    store_phone = models.CharField(verbose_name="Номер телефона магазина", max_length=15)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True, verbose_name="Область",)
    district = models.ForeignKey(District, on_delete=models.SET_NULL, null=True, verbose_name="Район или город")
    street = models.CharField(verbose_name="Улица и номер дома", max_length=200)
    lat = models.CharField(verbose_name="Latitude(широта)", max_length=100,
                           validators=[RegexValidator(
                                        regex='^([0-9.-]+).+?([0-9.-]+)',
                                        message='неверный цифра',
                                        code='invalid_latitude'
                                    )])
    lang = models.CharField(verbose_name="Longitude (долгота)", max_length=100,
                            validators=[RegexValidator(
                                        regex='^([0-9.-]+).+?([0-9.-]+)',
                                        message='неверный цифра',
                                        code='invalid_longitute'
                                    )])
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.store_name} - {self.store_phone}"


class Contact(models.Model):
    name = models.CharField(verbose_name="Имя", max_length=150)
    email = models.EmailField(verbose_name="Эмайл", null=True, blank=True)
    phoneNumber = models.CharField(verbose_name="Телефонный номер", max_length=14)
    text = models.TextField(verbose_name="Текст")
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Kimdan: {self.name} - Matn: {self.text[:100]}"


class WebInfoModel(models.Model):
    photoForCover = models.ImageField(verbose_name='Изображение для обложки',
                                      upload_to='products', null=True, blank=True)
    website = models.CharField(verbose_name="Название веб-сайта", max_length=100, null=True)
    facebook = models.CharField(verbose_name="Полная ссылка на профиль Facebook", max_length=200, null=True, blank=True)
    instagram = models.CharField(verbose_name="Полная ссылка на профиль в инстаграмм",
                                 max_length=200, null=True, blank=True)
    telegram = models.CharField(verbose_name="Телеграм-канал линк", max_length=200, null=True, blank=True)
    tiktok = models.CharField(verbose_name="Полная ссылка на профиль в Тик-ток", max_length=200, null=True, blank=True)
    youtube = models.CharField(verbose_name="Полная ссылка на профиль в Youtube", max_length=200, null=True, blank=True)
    phoneNumber = models.CharField(verbose_name="Телефонный номер", max_length=200, null=True, blank=True)
    shortInfoRu = models.CharField(verbose_name="Краткая информация в русском", max_length=200, null=True, blank=True)
    shortInfoUz = models.CharField(verbose_name="Краткая информация в узбекском", max_length=200, null=True, blank=True)

    def __str__(self):
        return self.website


class ClientReviews(models.Model):
    video_image = models.ImageField(verbose_name="обложка к видео", upload_to="reviewvideos", null=True, blank=True)
    video_file = models.FileField(verbose_name="видео файл", upload_to="reviewvideos", null=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.video_file


class ClientOnlineStores(models.Model):
    icon = models.ImageField(upload_to='products', null=True, blank=True)
    full_url = models.URLField()

    def __str__(self):
        return self.full_url
