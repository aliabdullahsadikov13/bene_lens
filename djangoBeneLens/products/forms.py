from django import forms
from .models import Category, Product, Store, ChangineTimeLenses, Brand, good_with, ClientReviews, ClientOnlineStores


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ("category", "brand_name", "productname", "sezon_uz", "sezon_ru", "sezon_en", "description_uz",
                  "description_ru", "description_en", "features_uz", "features_ru", "features_en", "productPhoto",
                  "photo_upakovka", "lensePhoto", "price", "ageGroup_ru", "ageGroup_uz", "ageGroup_en",
                  'time_to_change', "diameter", "radius", "humidity", "material_type_ru", "material_type_uz",
                  "material_type_en", "design_ru", "design_uz", "design_en", "time_of_use_uz",
                  "time_of_use_ru", "time_of_use_en", "number_of_blisters_uz", "number_of_blisters_ru",
                  "number_of_blisters_en", "country_uz", "country_ru", "country_en")
        widgets = {
            'features_uz': forms.TextInput(attrs={'placeholder': 'Plano, - 0.50 to -6.00 (0.25 steps)........'}),
            'features_ru': forms.TextInput(attrs={'placeholder': 'Plano, - 0.50 to -6.00 (0.25 steps)........'}),
            'features_en': forms.TextInput(attrs={'placeholder': 'Plano, - 0.50 to -6.00 (0.25 steps)........'}),
            'description_uz': forms.TextInput(attrs={'placeholder': 'Barcha tadbirlar uchun juda ....'}),
            'description_ru': forms.TextInput(attrs={'placeholder': 'Barcha tadbirlar uchun juda ....'}),
            'description_en': forms.TextInput(attrs={'placeholder': 'Barcha tadbirlar uchun juda ....'}),

        }


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ("categoryName_ru", "categoryName_uz", "categoryName_en",)


class BrandForm(forms.ModelForm):
    class Meta:
        model = Brand
        fields = ('brand_name', 'brand_text_uz', 'brand_text_ru', 'brand_text_en',)


class Good_with_form(forms.ModelForm):
    class Meta:
        model = good_with
        fields = ('product', 'name_uz', 'name_ru', 'name_en',)


class ChangingTimeLenseForm(forms.ModelForm):
    class Meta:
        model = ChangineTimeLenses
        fields = ['time_ru', 'time_uz', 'time_en', ]


class StoreForm(forms.ModelForm):
    class Meta:
        model = Store
        fields = ('store_name_ru', "store_name_uz", "store_name_en", "store_photo", "store_phone",
                  "region_ru", "district_ru", "region_uz", "district_uz", "region_en", "district_en",
                  "street_ru", "street_uz", "street_en", "lat", "lang",)


class VideoForm(forms.ModelForm):
    class Meta:
        model = ClientReviews
        fields = "__all__"


class OnlineStoresForm(forms.ModelForm):
    class Meta:
        model = ClientOnlineStores
        fields = "__all__"
