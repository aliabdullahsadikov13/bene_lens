# Generated by Django 4.0 on 2022-05-23 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0042_good_with_name_en_good_with_name_ru_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='country',
            field=models.CharField(max_length=100, null=True, verbose_name='Страна производства'),
        ),
        migrations.AddField(
            model_name='product',
            name='country_en',
            field=models.CharField(max_length=100, null=True, verbose_name='Страна производства'),
        ),
        migrations.AddField(
            model_name='product',
            name='country_ru',
            field=models.CharField(max_length=100, null=True, verbose_name='Страна производства'),
        ),
        migrations.AddField(
            model_name='product',
            name='country_uz',
            field=models.CharField(max_length=100, null=True, verbose_name='Страна производства'),
        ),
        migrations.AddField(
            model_name='product',
            name='design',
            field=models.CharField(max_length=100, null=True, verbose_name='Дизайн линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='design_en',
            field=models.CharField(max_length=100, null=True, verbose_name='Дизайн линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='design_ru',
            field=models.CharField(max_length=100, null=True, verbose_name='Дизайн линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='design_uz',
            field=models.CharField(max_length=100, null=True, verbose_name='Дизайн линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='diameter',
            field=models.CharField(max_length=10, null=True, verbose_name='Диаметр линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='diameter_en',
            field=models.CharField(max_length=10, null=True, verbose_name='Диаметр линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='diameter_ru',
            field=models.CharField(max_length=10, null=True, verbose_name='Диаметр линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='diameter_uz',
            field=models.CharField(max_length=10, null=True, verbose_name='Диаметр линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='features',
            field=models.CharField(max_length=200, null=True, verbose_name='доступные возможности'),
        ),
        migrations.AddField(
            model_name='product',
            name='features_en',
            field=models.CharField(max_length=200, null=True, verbose_name='доступные возможности'),
        ),
        migrations.AddField(
            model_name='product',
            name='features_ru',
            field=models.CharField(max_length=200, null=True, verbose_name='доступные возможности'),
        ),
        migrations.AddField(
            model_name='product',
            name='features_uz',
            field=models.CharField(max_length=200, null=True, verbose_name='доступные возможности'),
        ),
        migrations.AddField(
            model_name='product',
            name='humidity',
            field=models.CharField(max_length=10, null=True, verbose_name='Влажность'),
        ),
        migrations.AddField(
            model_name='product',
            name='humidity_en',
            field=models.CharField(max_length=10, null=True, verbose_name='Влажность'),
        ),
        migrations.AddField(
            model_name='product',
            name='humidity_ru',
            field=models.CharField(max_length=10, null=True, verbose_name='Влажность'),
        ),
        migrations.AddField(
            model_name='product',
            name='humidity_uz',
            field=models.CharField(max_length=10, null=True, verbose_name='Влажность'),
        ),
        migrations.AddField(
            model_name='product',
            name='material_type',
            field=models.CharField(max_length=100, null=True, verbose_name='Тип материала'),
        ),
        migrations.AddField(
            model_name='product',
            name='material_type_en',
            field=models.CharField(max_length=100, null=True, verbose_name='Тип материала'),
        ),
        migrations.AddField(
            model_name='product',
            name='material_type_ru',
            field=models.CharField(max_length=100, null=True, verbose_name='Тип материала'),
        ),
        migrations.AddField(
            model_name='product',
            name='material_type_uz',
            field=models.CharField(max_length=100, null=True, verbose_name='Тип материала'),
        ),
        migrations.AddField(
            model_name='product',
            name='number_of_blisters',
            field=models.CharField(max_length=100, null=True, verbose_name='Количество блистеров в упаковке'),
        ),
        migrations.AddField(
            model_name='product',
            name='number_of_blisters_en',
            field=models.CharField(max_length=100, null=True, verbose_name='Количество блистеров в упаковке'),
        ),
        migrations.AddField(
            model_name='product',
            name='number_of_blisters_ru',
            field=models.CharField(max_length=100, null=True, verbose_name='Количество блистеров в упаковке'),
        ),
        migrations.AddField(
            model_name='product',
            name='number_of_blisters_uz',
            field=models.CharField(max_length=100, null=True, verbose_name='Количество блистеров в упаковке'),
        ),
        migrations.AddField(
            model_name='product',
            name='radius',
            field=models.CharField(max_length=20, null=True, verbose_name='Радиус кривизны линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='radius_en',
            field=models.CharField(max_length=20, null=True, verbose_name='Радиус кривизны линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='radius_ru',
            field=models.CharField(max_length=20, null=True, verbose_name='Радиус кривизны линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='radius_uz',
            field=models.CharField(max_length=20, null=True, verbose_name='Радиус кривизны линзы'),
        ),
        migrations.AddField(
            model_name='product',
            name='time_of_use',
            field=models.CharField(max_length=100, null=True, verbose_name='Время использования'),
        ),
        migrations.AddField(
            model_name='product',
            name='time_of_use_en',
            field=models.CharField(max_length=100, null=True, verbose_name='Время использования'),
        ),
        migrations.AddField(
            model_name='product',
            name='time_of_use_ru',
            field=models.CharField(max_length=100, null=True, verbose_name='Время использования'),
        ),
        migrations.AddField(
            model_name='product',
            name='time_of_use_uz',
            field=models.CharField(max_length=100, null=True, verbose_name='Время использования'),
        ),
    ]
