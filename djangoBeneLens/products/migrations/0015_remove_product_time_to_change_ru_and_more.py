# Generated by Django 4.0 on 2022-04-24 16:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0014_product_time_to_change_ru_product_time_to_change_uz'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='time_to_change_ru',
        ),
        migrations.RemoveField(
            model_name='product',
            name='time_to_change_uz',
        ),
        migrations.AddField(
            model_name='changinetimelenses',
            name='time_ru',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='changinetimelenses',
            name='time_uz',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
