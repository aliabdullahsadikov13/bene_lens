# Generated by Django 4.0 on 2022-04-29 16:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0038_brand_brand_name_en_category_categoryname_en_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='store_photo',
            field=models.CharField(max_length=300, null=True, verbose_name='Ссылка на фото магазина'),
        ),
    ]
