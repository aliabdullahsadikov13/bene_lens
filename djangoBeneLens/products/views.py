from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, CreateView, DetailView, UpdateView, DeleteView

from django.views.generic.base import View

# from .forms import StoreForm, ProductForm, CategoryForm
from .forms import ProductForm, CategoryForm, StoreForm, ChangingTimeLenseForm, BrandForm, Good_with_form, VideoForm
from .models import Category, Product, Store, Contact, WebInfoModel, Region, District, ChangineTimeLenses, Brand, \
    good_with, ClientReviews, ClientOnlineStores


# Create your views here.
class DashHomeView(LoginRequiredMixin,TemplateView):
    template_name = 'dashboard/dashboard.html'
    login_url = 'login'


class DashAllCategoryView(LoginRequiredMixin,ListView):
    model = Category
    template_name = "dashboard/dashallCategory.html"
    context_object_name = 'allCategory'
    login_url = 'login'


class DashAddCategoryView(LoginRequiredMixin,CreateView):
    model = Category
    form_class = CategoryForm
    template_name = 'dashboard/dashAddCategory.html'
    success_url = reverse_lazy('dashboardAllCategory')
    login_url = 'login'


class DashCategoryDeleteView(LoginRequiredMixin,View):
    def get(self,request,pk):
        category = Category.objects.get(pk=pk)
        category.delete()
        return redirect('/dashboard/category/all')
    login_url = 'login'


class DashCategoryEditView(LoginRequiredMixin,UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = 'dashboard/dashcategoryEdit.html'
    success_url = reverse_lazy('dashboardAllCategory')
    login_url = 'login'


class ProductsAllView(LoginRequiredMixin,ListView):
    model = Product
    template_name = 'dashboard/allproducts.html'
    context_object_name = 'productslist'
    login_url = 'login'


class ProductDetailView(LoginRequiredMixin,DetailView):
    model = Product
    template_name = "dashboard/productDetail.html"
    login_url = 'login'


class ProductAddView(LoginRequiredMixin,CreateView):
    model = Product
    form_class = ProductForm
    template_name = "dashboard/productAdd.html"
    success_url = reverse_lazy('productList')
    login_url = 'login'


class ProductEditView(LoginRequiredMixin,UpdateView):
    model = Product
    form_class = ProductForm
    template_name = 'dashboard/productEdit.html'
    success_url = reverse_lazy('productList')
    login_url = 'login'


class ProductDeleteView(LoginRequiredMixin,DeleteView):
    model = Product
    template_name = 'dashboard/productdelete.html'
    success_url = reverse_lazy('productList')
    login_url = 'login'


class DashStoreAllView(LoginRequiredMixin,ListView):
    model = Store
    template_name = 'dashboard/dashstoreAll.html'
    context_object_name = 'allStore'
    login_url = 'login'


class DashStoreDetailView(LoginRequiredMixin,DetailView):
    model = Store
    template_name = 'dashboard/dashstoredetail.html'
    login_url = 'login'


class DashStoreAddView(LoginRequiredMixin,CreateView):
    model = Store
    form_class = StoreForm
    template_name = 'dashboard/dashstoreAdd.html'
    success_url = reverse_lazy('dashStore_all')
    login_url = 'login'


class DashStoreEditView(LoginRequiredMixin,UpdateView):
    model = Store
    form_class = StoreForm
    template_name = 'dashboard/dashstoreEdit.html'
    success_url = reverse_lazy('dashStore_all')
    login_url = 'login'


class DashStoreDeleteView(LoginRequiredMixin,DeleteView):
    model = Store
    template_name = 'dashboard/dashstoreDelete.html'
    success_url = reverse_lazy('dashStore_all')
    login_url = 'login'


class ContactFormAllView(LoginRequiredMixin,ListView):
    model = Contact
    template_name = 'dashboard/all_messages.html'
    context_object_name = 'allMessages'
    login_url = 'login'


class ContactDeleteView(LoginRequiredMixin,View):
    def get(self,request,pk):
        message = Contact.objects.get(pk=pk)
        message.delete()
        return redirect('/dashboard/messages/all')
    login_url = 'login'


class OnlaynMagazinAll(LoginRequiredMixin,ListView):
    model = WebInfoModel
    template_name = 'dashboard/companyContactAll.html'
    context_object_name = 'all_online_magazines'
    login_url = 'login'


class OnlaynMagazinUpdateView(LoginRequiredMixin,UpdateView):
    model = WebInfoModel
    template_name = 'dashboard/companyContactEdit.html'
    fields = '__all__'
    success_url = reverse_lazy('all_online_magazines')
    login_url = 'login'


class OnlaynMagazinCreateView(LoginRequiredMixin,CreateView):
    model = WebInfoModel
    template_name = 'dashboard/companyContactCreate.html'
    fields = '__all__'
    success_url = reverse_lazy('all_online_magazines')
    login_url = 'login'


class OnlaynMagazinDeleteView(LoginRequiredMixin,View):
    def get(self,request,pk):
        magaz_name = WebInfoModel.objects.get(pk=pk)
        magaz_name.delete()
        return redirect('all_online_magazines')
    login_url = 'login'


class DashChangeTimeAll(LoginRequiredMixin,ListView):
    model = ChangineTimeLenses
    template_name = "dashboard/timeTochange.html"
    context_object_name = 'allTimeChanges'
    login_url = 'login'


class DashAddChangeTimeView(LoginRequiredMixin,CreateView):
    model = ChangineTimeLenses
    form_class = ChangingTimeLenseForm
    template_name = 'dashboard/TimetochangeAdd.html'
    success_url = reverse_lazy('ChangtingTime')
    login_url = 'login'


class DashChangeTimeDeleteView(LoginRequiredMixin,View):
    def get(self,request,pk):
        change_time = ChangineTimeLenses.objects.get(pk=pk)
        change_time.delete()
        return redirect('/dashboard/changingtime/')
    login_url = 'login'


class DashChangeTimeEditView(LoginRequiredMixin,UpdateView):
    model = ChangineTimeLenses
    form_class = ChangingTimeLenseForm
    template_name = 'dashboard/TimetochangeEdit.html'
    success_url = reverse_lazy('ChangtingTime')
    login_url = 'login'


class DashBrandAll(LoginRequiredMixin, ListView):
    model = Brand
    template_name = "dashboard/dashbrandAll.html"
    context_object_name = 'all_brands'
    login_url = 'login'


class DashBrandAdd(LoginRequiredMixin, CreateView):
    model = Brand
    form_class = BrandForm
    template_name = 'dashboard/dashbrandAdd.html'
    success_url = reverse_lazy('dash_brand_all')
    login_url = 'login'


class DashBrandDelete(LoginRequiredMixin, View):
    def get(self, request, pk):
        brand_name = Brand.objects.get(pk=pk)
        brand_name.delete()
        return redirect('dash_brand_all')
    login_url = 'login'


class DashBrandEdit(LoginRequiredMixin, UpdateView):
    model = Brand
    form_class = BrandForm
    template_name = 'dashboard/dashbrandEdit.html'
    success_url = reverse_lazy('dash_brand_all')
    login_url = 'login'


class Good_with_list(LoginRequiredMixin, ListView):
    model = good_with
    template_name = "dashboard/good_with_list.html"
    context_object_name = 'all_good_with_items'
    login_url = 'login'


class Good_with_add_view(LoginRequiredMixin, CreateView):
    model = good_with
    form_class = Good_with_form
    template_name = 'dashboard/good_with_add.html'
    success_url = reverse_lazy('good_with_list')
    login_url = 'login'


class Good_with_delete_view(LoginRequiredMixin, View):
    def get(self, request, pk):
        goodwith = good_with.objects.get(pk=pk)
        goodwith.delete()
        return redirect('good_with_list')
    login_url = 'login'


class Good_with_edit_view(LoginRequiredMixin, UpdateView):
    model = good_with
    form_class = Good_with_form
    template_name = 'dashboard/good_with_edit.html'
    success_url = reverse_lazy('good_with_list')
    login_url = 'login'


def load_districts_ru(request):
    region_id = request.GET.get('region_ru')
    print(region_id)
    districts_ru = District.objects.filter(region_id=region_id).order_by('name_ru')
    return render(request, 'dropdown/city_dropdown_list_options.html', {'districts_ru': districts_ru})


def fill_region_uz(request):
    region_id = request.GET.get('region_ru')
    region_uz = Region.objects.filter(id=region_id).order_by('name_uz')
    context = {
        'region_uz': region_uz,
    }
    return render(request,'dropdown/region_uz_dropdown.html', context)


def fill_district_uz(request):
    district_id = request.GET.get("district_ru")
    district_uz = District.objects.filter(id=district_id)
    context = {
        'district_uz': district_uz
    }
    return render(request,'dropdown/district_uz_dropdown.html', context)


class ReviewListView(LoginRequiredMixin, ListView):
    model = ClientReviews
    template_name = "dashboard/dashReviewAll.html"
    context_object_name = 'all_reviews'
    login_url = 'login'


class ReviewAddView(LoginRequiredMixin, CreateView):
    model = ClientReviews
    form_class = VideoForm
    template_name = 'dashboard/dashReviewAdd.html'
    success_url = reverse_lazy('dash_review_all')
    login_url = 'login'


class ReviewDeleteView(LoginRequiredMixin, View):

    def get(self, request, pk):
        review_name = ClientReviews.objects.get(pk=pk)
        review_name.delete()
        return redirect('dash_review_all')
    login_url = 'login'


class ReviewEditView(LoginRequiredMixin, UpdateView):
    model = ClientReviews
    form_class = VideoForm
    template_name = 'dashboard/dashReviewEdit.html'
    success_url = reverse_lazy('dash_review_all')
    login_url = 'login'


class StoreOnlineAllView(LoginRequiredMixin, ListView):
    model = ClientOnlineStores
    template_name = 'dashboard/stores_online_all.html'
    context_object_name = 'stores_all'
    login_url = 'login'


class StoreOnlineUpdateView(LoginRequiredMixin, UpdateView):
    model = ClientOnlineStores
    template_name = 'dashboard/stores_online_update.html'
    fields = '__all__'
    success_url = reverse_lazy('all_store_online')
    login_url = 'login'


class StoreOnlineCreateView(LoginRequiredMixin, CreateView):
    model = ClientOnlineStores
    template_name = 'dashboard/stores_online_create.html'
    fields = '__all__'
    success_url = reverse_lazy('all_store_online')
    login_url = 'login'


class StoreOnlineDeleteView(LoginRequiredMixin, View):
    def get(self, request, pk):
        magaz_name = ClientOnlineStores.objects.get(pk=pk)
        magaz_name.delete()
        return redirect('all_store_online')
    login_url = 'login'