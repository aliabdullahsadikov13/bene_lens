from modeltranslation.translator import translator, TranslationOptions
from .models import Region, District, Store, Category, Product, ChangineTimeLenses, Brand, good_with


class RegionTranslationOptions(TranslationOptions):
    fields = ('name', )


class DistrictTranslationOptions(TranslationOptions):
    fields = ('name',)


class StoreTranslationOption(TranslationOptions):
    fields = ('store_name', 'region', 'district', 'street')


class CategoryTranslationOption(TranslationOptions):
    fields = ('categoryName',)


class ProductTranslationOption(TranslationOptions):
    fields = ('sezon', 'description', 'ageGroup', 'features', 'time_of_use',
              'number_of_blisters', 'material_type', 'design', 'country')


class ChangingLensesTranslationOption(TranslationOptions):
    fields = ('time',)


class BrandTranslationOption(TranslationOptions):
    fields = ('brand_text',)


class Good_withTranslationOption(TranslationOptions):
    fields = ('name',)


translator.register(Region, RegionTranslationOptions)
translator.register(District, DistrictTranslationOptions)
translator.register(Store, StoreTranslationOption)
translator.register(Category, CategoryTranslationOption)
translator.register(Product, ProductTranslationOption)
translator.register(ChangineTimeLenses, ChangingLensesTranslationOption)
translator.register(Brand, BrandTranslationOption)
translator.register(good_with, Good_withTranslationOption)
