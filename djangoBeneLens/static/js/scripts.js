function MobileMenu() {
  var mobileMenu = $(".mobile-menu"),
      toggleMenuElement = $(".js-mobile-menu-toggle");

  this.open = function () {
      toggleMenuElement.addClass("open");
      mobileMenu.addClass("open");
      if (mobileMenu.hasClass("mobile-menu--main")) {
          _html.addClass("overflow-hidden--lg mobile-open");
      } else if (mobileMenu.hasClass("mobile-menu--landing")) {
          _html.addClass("overflow-hidden--sm mobile-open");
      } else if (mobileMenu.hasClass("mobile-menu--dashboard")) {
          _html.addClass("overflow-hidden--1339 mobile-open");
      } else {
          _html.addClass("overflow-hidden mobile-open");
      }
  };

  this.close = function () {
      toggleMenuElement.removeClass("open");
      mobileMenu.removeClass("open");
      if (mobileMenu.hasClass("mobile-menu--main")) {
          _html.removeClass("overflow-hidden--lg mobile-open");
      } else if (mobileMenu.hasClass("mobile-menu--landing")) {
          _html.removeClass("overflow-hidden--sm mobile-open");
      } else if (mobileMenu.hasClass("mobile-menu--dashboard")) {
          _html.removeClass("overflow-hidden--1339 mobile-open");
      } else {
          _html.removeClass("overflow-hidden mobile-open");
      }
  };

  this.init = function () {
      toggleMenuElement.on(_touchTap, function (e) {
          e.preventDefault();
          toggleMenuElement.toggleClass("open");
          mobileMenu.toggleClass("open");
          if (mobileMenu.hasClass("mobile-menu--main")) {
              _html.toggleClass("overflow-hidden--lg mobile-open");
          } else if (mobileMenu.hasClass("mobile-menu--landing")) {
              _html.toggleClass("overflow-hidden--sm mobile-open");
          } else if (mobileMenu.hasClass("mobile-menu--dashboard")) {
              _html.toggleClass("overflow-hidden--1339 mobile-open");
          } else {
              _html.toggleClass("overflow-hidden mobile-open");
          }
          return false;
      });
  };
}

function InitMap() {
  let map,
      addressArray = [],
      markers = [],
      here,
      mapsCard;
      const locationButton = document.getElementById("button");

      

  this.init = function () {
      if($(".pg--store").length) {

          map = new google.maps.Map(document.getElementById("map"), {
              center: { lat: 41.3099786206113, lng: 69.27868360279695 },
              zoom: 10,
          });

          function takeAttrData() {
              addressArray = [];
              mapsCard = $(".js-address-item")
              mapsCard.each(function (indx, element) {
                  let lat = $(this).attr("data-address-lat");
                  let lng = $(this).attr("data-address-lng");
                  addressArray.push({
                      lat: lat,
                      lng: lng,
                  });
              });
          }

          function addMarkerMap() {
              for (let i = 0; i < addressArray.length; i++) {
                  const marker = new google.maps.Marker({
                      position: {
                          lat: +addressArray[i].lat,
                          lng: +addressArray[i].lng,
                      },
                      map,
                  });
                  markers.push(marker);
              }
          }

          function setMapOnAll(map) {
              for (let i = 0; i < markers.length; i++) {
                  markers[i].setMap(map);
              }
              markers = [];
          }

          infoWindow = new google.maps.InfoWindow();

          takeAttrData();
          addMarkerMap();

          locationButton.addEventListener("click", (e) => {
              e.preventDefault();
              if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(
                      (position) => {
                          here = {
                              lat: position.coords.latitude,
                              lng: position.coords.longitude,
                          };

                          let optimalAddress = [];
                          let numb = 0;

                          for (let i = 0; i < addressArray.length; i++) {
                              let a, b, c;

                              a = Math.pow(
                                  Math.abs(+here.lat - +addressArray[i].lat),
                                  2
                              );
                              b = Math.pow(
                                  Math.abs(+here.lng - +addressArray[i].lng),
                                  2
                              );
                              c = Math.sqrt(a + b);
                              if (c > numb && numb == 0) {
                                  numb = c;
                                  optimalAddress.splice(0);
                                  optimalAddress.push(
                                      addressArray[i].lat,
                                      addressArray[i].lng
                                  );
                              } else if (c < numb) {
                                  numb = c;
                                  optimalAddress.splice(0);
                                  optimalAddress.push(
                                      addressArray[i].lat,
                                      addressArray[i].lng
                                  );
                              }
                          }

                          const pos = {
                              lat: +optimalAddress[0],
                              lng: +optimalAddress[1],
                          };

                          mapsCard.each(function (indx, element) {
                              if (
                                  $(this).attr("data-address-lat") !=
                                      optimalAddress[0] &&
                                  $(this).attr("data-address-lng") !=
                                      optimalAddress[1]
                              ) {
                                  $(this).remove();
                              }
                          });
                          if($(window).innerWidth() < 576) {
                            window.addressSwiper.updateSlides();
                          }
                          infoWindow.open(map);
                          map.setCenter(pos);
                          map.setZoom(15);
                      },
                      () => {
                          handleLocationError(true, infoWindow, map.getCenter());
                      }
                  );
              } else {
                  handleLocationError(false, infoWindow, map.getCenter());
              }
              return false;
          });
          function handleLocationError(browserHasGeolocation, infoWindow, pos) {
              infoWindow.setPosition(pos);
              infoWindow.setContent(
                  browserHasGeolocation
                      ? "Error: The Geolocation service failed."
                      : "Error: Your browser doesn't support geolocation."
              );
              infoWindow.open(map);
          }
      }
  };

  this.addMarker = function () {
      function takeAttrData() {
          addressArray = [];
          mapsCard = $(".js-address-item")
          mapsCard.each(function (indx, element) {
              let lat = $(this).attr("data-address-lat");
              let lng = $(this).attr("data-address-lng");
              addressArray.push({
                  lat: lat,
                  lng: lng,
              });
          });
      }

      function addMarkerMap() {
          for (let i = 0; i < addressArray.length; i++) {
              const marker = new google.maps.Marker({
                  position: {
                      lat: +addressArray[i].lat,
                      lng: +addressArray[i].lng,
                  },
                  map,
              });
              markers.push(marker);
          }
      }

      function setMapOnAll(map) {
          for (let i = 0; i < markers.length; i++) {
              markers[i].setMap(map);
          }
          markers = [];
      }
      setMapOnAll(null);
      takeAttrData();
      addMarkerMap();
      return false;
  };
}

function IsFilled() {
  var self = this;

  this.checkField = function (field) {
      var field = $(field),
          formField = field.closest('.field');

      if (field.val() !== '') {
          formField.addClass('filled');
      } else {
          formField.removeClass('filled');
      }
  };

  this.checkSelect = function (select) {
      var select = $(select),
          selectField = select.closest('.field');

      if (select.val() !== '') {
          $(select.data('select2').$container).addClass('filled');
          selectField.addClass('filled');
      }
  };

  this.checkAllFields = function (blockClass) {
      var formsFields;
      var selects;

      if (blockClass !== undefined) {
          formsFields = $(blockClass).find('.input, .textarea');
          selects = $(blockClass).find('.select2');
      } else {
          formsFields = $('.input, .textarea');
          selects = $('.select2');
      }

      formsFields.each(function (i) {
          self.checkField(formsFields[i]);
      });

      formsFields.on('input change', function () {
          self.checkField(this);
      });

      selects.each(function (i) {
          self.checkSelect(selects[i]);
      });

      selects.on('change', function () {
          self.checkSelect(this);
      });
  };

  this.init = function () {
      this.checkAllFields();
  };
}
////=include vendor/formMessages.js

function invertibleHeader() {
_header.toggleClass("header--sticky", _window.scrollTop() > 20);
// $('.mobile-menu').toggleClass('mobile-menu--sticky', _window.scrollTop() > 50);
_window.on("scroll", function () {
  _header.toggleClass("header--sticky", _window.scrollTop() > 20);
  // $('.mobile-menu').toggleClass('mobile-menu--sticky', _window.scrollTop() > 50);
});
}

function initSelect2() {
$(".select2-single").each(function () {
  var _this = $(this),
    _thisDataClass = _this.data("class"),
    _thisPlaceholder = _this.data("placeholder") || "";
  _this.show();
  _this.select2({
    width: "100%",
    minimumResultsForSearch: Infinity,
    placeholder: _thisPlaceholder,
    containerCssClass: _thisDataClass,
    dropdownCssClass: _thisDataClass,
  });
  _this.val(null);
  _this.trigger("change");
});
}

function initAccordion() {
_document.on(_touchTap, ".js-accordion-trigger", function (e) {
  e.preventDefault();
  var _this = $(this);
  var _thisItem = _this.closest(".js-accordion-item");
  var _thisItemParent = _thisItem.parent();
  var _thisContent = _thisItem.find(".js-accordion-content");

  if (_thisItem.hasClass("active")) {
    _thisItemParent.find(".js-accordion-item").removeClass("active");
    _thisContent.slideUp(200);
  } else {
    _thisItemParent.find(".js-accordion-item").removeClass("active");
    _thisItemParent.find(".js-accordion-content").slideUp(200);
    _thisItem.addClass("active");
    _thisContent.slideDown(400);
  }
});
}

function InitSliders() {
var catalogGrid = new Swiper(".katalog-grid__item-image", {
  slidesPerView: 1,
  centeredSlides: true,
  spaceBetween: 25,
  loop: true,
  pagination: {
    el: ".katalog-grid__slider-pagination",
    clickable: true,
  },
  breakpoints: {
    576: {
      navigation: {
        nextEl: ".katalog-grid__slider-next",
        prevEl: ".katalog-grid__slider-prev",
      },
    },
  },
});

this.init = function () {
  _document.on("ready", function () {
    if (window.innerWidth <= 576) {
      var feature = new Swiper(".feature-slider", {
        slidesPerView: 1,
        spaceBetween: 15,
        centeredSlides: true,
        speed: 1000,
        autoplay: {
          delay: 1500,
          disableOnInteraction: false,
        },
        loop: true,
        navigation: {
          nextEl: ".feature__mobile-next",
          prevEl: ".feature__mobile-prev",
        },
      });

      $(".product-slider__swiper").addClass("swiper-wrapper");
      $(".product-slider__item").addClass("swiper-slide");
      var productSlider = new Swiper(".product-slider__inner", {
        slidesPerView: 1.5,
        spaceBetween: 12,
        centeredSlides: true,
        autoplay: {
          delay: 1500,
        },
        loop: true,
        pagination: {
          el: ".product-slider__pagination",
          clickable: true,
        },
      });

      $(".address__item-list-cards").addClass("swiper-wrapper");
      $(".address__item").addClass("swiper-slide");
      $(".address__item-list-map").remove();
      window.addressSwiper = new Swiper(".address__item-list", {
        slidesPerView: 1,
        spaceBetween: 12,
        centeredSlides: true,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
      });


      $(".why-feature__list").addClass("swiper-wrapper");
      $(".why-feature__item").addClass("swiper-slide");
      var whyFeatureSwiper = new Swiper(".why-feature__inner", {
        slidesPerView: 1,
        centeredSlides: true,
        spaceBetween: 12,
        autoplay: {
          delay: 1500,
        },
        loop: true,
      });
    }
  });

  var reviewsSlider = new Swiper(".reviews-slider", {
    slidesPerView: 1.2,
    spaceBetween: 32,
    pagination: {
      el: ".reviews-pagination",
      clickable: true,
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
    },
  });

  var productImageSlider = new Swiper(".product__left-slider", {
    slidesPerView: 1,
    spaceBetween: 25,
    centeredSlides: true,
    pagination: {
      el: ".product__left-pagination",
      clickable: true,
    },
    // breakpoints: {
    //   991: {
    //     spaceBetween: 0,
    //   },
    // },
  });

  var leadSlider = new Swiper(".lead-swiper", {
    slidesPerView: 1,
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 1500,
      reverseDirection: true,
      disableOnInteraction: false,
    },
  });

  var catalogGrid = new Swiper(".katalog-grid__item-image", {
    slidesPerView: 1,
    centeredSlides: true,
    spaceBetween: 25,
    loop: true,
    type: "custom",
    renderCustom: function (swiper, current, total) {
      var out = "";
      for (i = 1; i < total + 1; i++) {
        if (i == current) {
          out =
            out +
            '<span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex=' +
            i +
            ' role="button" aria-label="Go to slide ' +
            i +
            1 +
            '"></span>';
        } else {
          out =
            out +
            '<span class="swiper-pagination-bullet" tabindex=' +
            i +
            ' role="button" aria-label="Go to slide ' +
            i +
            1 +
            '"></span>';
        }
      }
      return out;
    },
    breakpoints: {
      576: {
        navigation: {
          nextEl: ".katalog-grid__slider-next",
          prevEl: ".katalog-grid__slider-prev",
        },
      },
    },
  });
};

this.update = function () {
  console.log(catalogGrid);
  catalogGrid.forEach(function (element) {
    element.update();
  });
};
}

function testWebP(callback) {
var webP = new Image();
webP.onload = webP.onerror = function () {
  callback(webP.height == 2);
};
webP.src =
  "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}

function addCircleColor() {
if ($(".choose-lenses").length) {
  const color = [
    "#612D86",
    "#8EC642",
    "#EE642A",
    "#612D86",
    "#8EC642",
    "#EE642A",
  ];
  var $cards = $(".choose-lenses__left-top");

  $cards.each(function (indx, element) {
    var $cardsLi = $(element).find(".choose-lenses__color-circle");
    $cardsLi.each(function (indx, element) {
      $(element).css("backgroundColor", `${color[indx]}`);
    });
  });
}
}

function imitationClickAddress() {
$(".js-address-actions").on("click", function (e) {
  e.preventDefault();
  let phone = document.querySelector(
    ".address__item.swiper-slide-active .address__item-phone a"
  );
  phone.click();
});
$(document).on("click", ".address__item-phone", function (e) {
  let phone = document.querySelector(
    ".address__item.swiper-slide-active .address__item-phone a"
  );
  phone.click();
});
}

function chooseColorLenses() {
if ($(".choose-lenses").length != 0) {
  let tab = $(".js-choose-color");
  let slide = $(".js-lenses-slide");

  slide.remove();

  tab.each(function (indx, element) {
    let bool = $(this).hasClass("active");
    let _this = $(this);
    slide.each(function (indx, element) {
      if (
        $(this).attr("data-lenses-slide") ==
          _this.attr("data-choose-color") &&
        bool
      ) {
        $(".lens-wrapper").append($(this)[0].outerHTML);
      }
    });
  });

  var lenses = new Swiper(".lens-slider", {
    slidesPerView: "8.5",
    spaceBetween: 24,
    centeredSlides: true,
    // loop: true,
    // freeMode: true,
    // observer: true,
    breakpoints: {},
  });

  let slideActive = $(".js-lenses-slide.swiper-slide-active"),
    slideActiveBg = $(".js-lenses-slide.swiper-slide-active").attr(
      "data-bg-color"
    ),
    slideActiveColor = $(".js-lenses-slide.swiper-slide-active").attr(
      "data-color-text"
    );

  $(".choose-lenses__left-top").hide();
  $(`.choose-lenses__left-top[data-show = ${slideActive.attr("data-show")}]`)
    .show()
    .css({
      background: `rgba(${slideActiveBg})`,
      color: `rgb(${slideActiveColor})`,
    })
    .addClass("open");

  _document.on("click", ".js-lenses-slide", function () {
    $(".choose-lenses__left-top").hide().removeClass("open");
    slideActive = $(".js-lenses-slide.swiper-slide-active");
    slideActiveBg = $(".js-lenses-slide.swiper-slide-active").attr(
      "data-bg-color"
    );
    slideActiveColor = $(".js-lenses-slide.swiper-slide-active").attr(
      "data-color-text"
    );
    $(
      `.choose-lenses__left-top[data-show = ${slideActive.attr("data-show")}]`
    )
      .show()
      .css({
        background: `rgba(${slideActiveBg})`,
        color: `rgb(${slideActiveColor})`,
      })
      .addClass("open");
  });

  $(".js-lenses-slide").on("click", function () {
    lenses.slideTo(lenses.clickedIndex);
    $(".choose-lenses__lens img").attr(
      "src",
      $(".js-lenses-slide.swiper-slide-active img").attr("src")
    );
  });

  $(".choose-lenses__lens img").attr(
    "src",
    $(".js-lenses-slide.swiper-slide-active img").attr("src")
  );

  $(".choose-lenses__select-color p").hide();
  $(
    `.choose-lenses__select-color p[data-show = ${$(
      ".js-choose-color.active"
    ).attr("data-choose-color")}]`
  ).show();

  $(".js-choose-color").on("click", function (e) {
    e.preventDefault();
    $(".js-choose-color").removeClass("active");
    $(this).addClass("active");

    $(".choose-lenses__select-color p").hide();
    $(
      `.choose-lenses__select-color p[data-show = ${$(this).attr(
        "data-choose-color"
      )}]`
    ).show();

    lenses.removeAllSlides();

    tab.each(function (indx, element) {
      let bool = $(this).hasClass("active");
      let _this = $(this);
      slide.each(function (indx, element) {
        if (
          $(this).attr("data-lenses-slide") ==
            _this.attr("data-choose-color") &&
          bool
        ) {
          $(".lens-wrapper").append($(this)[0].outerHTML);
        }
      });
    });

    lenses.update();
    $(".js-lenses-slide").on("click", function () {
      lenses.slideTo(lenses.clickedIndex);
      $(".choose-lenses__lens img").attr(
        "src",
        $(".swiper-slide-active img").attr("src")
      );
    });
  });

  lenses.update();
}
}

function chooseSex() {
$(".js-choose-sex").on("click", function (e) {
  e.preventDefault();
  $(".js-choose-sex").removeClass("active");
  $(this).addClass("active");
  if ($(this).text() == "Ayol" || $(this).text() == "Woman" || $(this).text() == "Женщина") {
    $(".choose-lenses__img .js-bg-img")
      .animate({ opacity: "hide" }, 0)
      .delay(200)
      .attr("src", "/static/images/female.png")
        .attr("srcset", "/static/images/female@2x.png 1.5x")
      .animate({ opacity: "show" }, 200);
    $(".choose-lenses__img source").attr(
      "srcset",
      "/static/images/female.webp, /static/images/female@2x.webp 1.5x"
    );
    $(".choose-lenses__lens")
      .animate({ opacity: "hide" }, 0)
      .removeClass("male")
      .delay(200)
      .addClass("female")
      .animate({ opacity: "show" }, 200);
  } else if ($(this).text() == "Erkak" || $(this).text() == "Man" || $(this).text() == "Мужчина") {
    $(".choose-lenses__img .js-bg-img")
      .animate({ opacity: "hide" }, 0)
      .delay(200)
      .attr("src", "/static/images/male")
      .attr("srcset", "/static/images/male@2x.png 1.5x")
      .animate({ opacity: "show" }, 200);
    $(".choose-lenses__img source").attr(
      "srcset",
      "/static/images/male.webp, /static/images/male@2x.webp 1.5x"
    );
    $(".choose-lenses__lens")
      .animate({ opacity: "hide" }, 0)
      .removeClass("female")
      .delay(200)
      .addClass("male")
      .animate({ opacity: "show" }, 200);
  }
});
}

function videoPlay() {
window.addEventListener("load", () => {
  let video = document.getElementById("vid");
  if (video != null) {
    video.play();
  }
});
}

function chooseAddress() {
if ($(".js-choose-address")) {
  $(".js-online").hide();
  let buttonPhone = $(".js-address-actions");
  $(".js-choose-address").on("click", function (e) {
    e.preventDefault();
    $(".js-choose-address").removeClass("active");
    $(this).addClass("active");
    if ($(".js-choose-address:eq(0)").hasClass("active")) {
      $(".js-offline").hide();
      $(".address__choose").hide();
      $(".js-online").show();
      buttonPhone.hide();
    } else {
      $(".js-online").hide();
      $(".address__choose").show();
      $(".js-offline").show();
      buttonPhone.show();
    }
  });
}
}

function openImage() {
$(function () {
  $(".minimized").click(function (event) {
    var i_path = $(this).attr("src");
    $("body").append(
      '<div id="overlay1"></div><div id="magnify"><img src="' +
        i_path +
        '"><div id="close-popup"><i></i></div></div>'
    );
    $("#magnify").css({
      position: "fixed",
      top: 0,
    });
    $("#overlay1, #magnify").fadeIn("fast");
  });

  $("body").on("click", "#magnify", function (event) {
    event.preventDefault();

    $("#overlay1, #magnify").fadeOut("fast", function () {
      $("#close-popup, #magnify, #overlay1").remove();
    });
  });
});
}

function mobileSubMenu() {
$(".js-mobile-menu__button").hide();
$(".mobile-menu__list:eq(1)").hide();
$(".js-open-mobileSubMenu").on("click", function (e) {
  e.preventDefault();
  $(".mobile-menu__list:eq(0)").animate({ opacity: "hide" }, 300).hide();
  $(".mobile-menu__list:eq(1)")
    .css("margin-top", "0")
    .animate({ opacity: "show" }, 300)
    .show();
  $(".js-mobile-menu__button").show();
});
$(".js-mobile-menu__button").on("click", function (e) {
  e.preventDefault();
  $(".js-mobile-menu__button").hide();
  $(".mobile-menu__list:eq(1)").animate({ opacity: "hide" }, 300).hide();
  $(".mobile-menu__list:eq(0)").animate({ opacity: "show" }, 300).show();
});
}

function calculatorTabs() {
$(".js-torik").hide();
$(".js-calculator-tab").on("click", function (e) {
  e.preventDefault();
  $(".js-calculator-tab").removeClass("active");
  $(this).addClass("active");

  if ($(this).attr("data-tab") == "torik") {
    $(".js-torik").slideDown();
  } else if ($(this).attr("data-tab") == "sferik") {
    $(".js-torik").slideUp();
  }
});
}

function calculate() {
$(document).ready(function () {
  let btn = $("#calculator_btn");

  btn.on("click", function (e) {
    e.preventDefault();

    let sphereLeft = $("#sphere-left").find(":selected").attr("value");
    let sphereRight = $("#sphere-right").find(":selected").attr("value");
    let cylLeft = $("#cyl-left").find(":selected").attr("value");
    let cylRight = $("#cyl-right").find(":selected").attr("value");
    let axiLeft = $("#axi-left").find(":selected").attr("value");
    let axiRight = $("#axi-right").find(":selected").attr("value");
    let sferik = $(".js-calculator-sferik");
    let torik = $(".js-calculator-torik");

    //sphereLeft
    if (sphereLeft >= -14 && sphereLeft <= -12.75) {
      sphereLeft -= -2.0;
    } else if (sphereLeft >= -12.5 && sphereLeft <= -10.75) {
      sphereLeft -= -1.5;
    } else if (sphereLeft >= -10.5 && sphereLeft <= -7.75) {
      sphereLeft -= -1.0;
    } else if (sphereLeft >= -7.5 && sphereLeft <= -5.75) {
      sphereLeft -= -0.5;
    } else if (sphereLeft >= -5.5 && sphereLeft <= -4.0) {
      sphereLeft -= -0.25;
    } else if (sphereLeft >= -3.75 && sphereLeft <= 3.0) {
      sphereLeft = +sphereLeft;
    } else if (sphereLeft >= 3.25 && sphereLeft <= 5.0) {
      sphereLeft = +sphereLeft + 0.25;
    } else if (sphereLeft >= 5.25 && sphereLeft <= 5.5) {
      sphereLeft = +sphereLeft + 0.5;
    } else if (sphereLeft == 0) {
      sphereLeft = 0;
    }

    //cylLeft
    if (cylLeft == -3.25 || cylLeft == -2.75 || cylLeft == -0.5) {
      cylLeft -= -0.5;
    } else if (
      cylLeft == -3.0 ||
      cylLeft == -2.5 ||
      cylLeft == -2.0 ||
      cylLeft == -1.5 ||
      cylLeft == -1.0 ||
      cylLeft == -0.25
    ) {
      cylLeft -= -0.25;
    } else if (
      cylLeft == -2.25 ||
      cylLeft == -1.75 ||
      cylLeft == -1.25 ||
      cylLeft == -0.75 ||
      cylLeft == 0
    ) {
      cylLeft = cylLeft;
    }

    //sphereRight
    if (sphereRight >= -14 && sphereRight <= -12.75) {
      sphereRight -= -2.0;
    } else if (sphereRight >= -12.5 && sphereRight <= -10.75) {
      sphereRight -= -1.5;
    } else if (sphereRight >= -10.5 && sphereRight <= -7.75) {
      sphereRight -= -1.0;
    } else if (sphereRight >= -7.5 && sphereRight <= -5.75) {
      sphereRight -= -0.5;
    } else if (sphereRight >= -5.5 && sphereRight <= -4.0) {
      sphereRight -= -0.25;
    } else if (sphereRight >= -3.75 && sphereRight <= 3.0) {
      sphereRight = +sphereRight;
    } else if (sphereRight >= 3.25 && sphereRight <= 5.0) {
      sphereRight = +sphereRight + 0.25;
    } else if (sphereRight >= 5.25 && sphereRight <= 5.5) {
      sphereRight = +sphereRight + 0.5;
    } else if (sphereRight == 0) {
      sphereRight = 0;
    }

    //cylRight
    if (cylRight == -3.25 || cylRight == -2.75 || cylRight == -0.5) {
      cylRight -= -0.5;
    } else if (
      cylRight == -3.0 ||
      cylRight == -2.5 ||
      cylRight == -2.0 ||
      cylRight == -1.5 ||
      cylRight == -1.0 ||
      cylRight == -0.25
    ) {
      cylRight -= -0.25;
    } else if (
      cylRight == -2.25 ||
      cylRight == -1.75 ||
      cylRight == -1.25 ||
      cylRight == -0.75 ||
      cylRight == 0
    ) {
      cylRight = cylRight;
    }

    if (sferik.hasClass("active")) {
      if (
        (sphereLeft || sphereLeft == 0) &&
        (sphereRight || sphereRight == 0)
      ) {
        $(".js-answerSphereLeft").text(sphereLeft);
        $(".js-answerSphereRight").text(sphereRight);
      } else {
        alert("Please Select Spherical value.");
      }
    }

    if (torik.hasClass("active")) {
      if (
        (sphereLeft || sphereLeft == 0) &&
        (sphereRight || sphereRight == 0) &&
        (cylLeft || cylLeft == 0) &&
        (cylRight || cylRight == 0) &&
        axiLeft &&
        axiRight
      ) {
        $(".js-answerSphereLeft").text(sphereLeft);
        $(".js-answerSphereRight").text(sphereRight);

        $(".js-answerCylinderLeft").text(cylLeft);
        $(".js-answerCylinderRight").text(cylRight);

        $(".js-answerAxisLeft").text(axiLeft);
        $(".js-answerAxisRight").text(axiRight);
      } else {
        alert("Please Select Spherical value.");
      }
    }
  });
});
}

function showContent() {
$(".about-grid__content").hide();
$(".js-about-grid-btn").on("click", function (e) {
  e.preventDefault();
  $(this)
    .toggleClass("active")
    .parent()
    .children(".about-grid__title")
    .toggleClass("active")
    .parent()
    .children(".about-grid__content")
    .slideToggle();
});
}

function App() {
return {
  mobileMenu: new MobileMenu(),
  isFilled: new IsFilled(),
  initMap: new InitMap(),
  initSlider: new InitSliders(),
  init: function () {
    invertibleHeader();
    this.mobileMenu.init();
    this.isFilled.init();
    this.initMap.init();
    this.initSlider.init();
    initSelect2();
    initAccordion();
    chooseColorLenses();
    chooseSex();
    videoPlay();
    chooseAddress();
    mobileSubMenu();
    calculatorTabs();
    openImage();
    calculate();
    showContent();
    imitationClickAddress();
    addCircleColor();
  },
};
}

testWebP(function (support) {
if (support == true) {
} else {
}
});

_document.ready(function () {
app = new App();
app.init();

_document.on("click", 'a[href*="#"]:not([href="#"])', function (e) {
  var _headerHeight = _header.height(),
    _thisHref = $(this).attr("href"),
    _sectionId = _thisHref.substr(_thisHref.indexOf("#")),
    _thisSection = $(_sectionId);

  app.mobileMenu.close();
  if (_thisSection.length > 0) {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: _thisSection.offset().top - _headerHeight,
      },
      500
    );
    return false;
  }
});

$("[data-mask]").each(function () {
  $(this).mask($(this).attr("data-mask"));
});

autosize($(".textarea"));

_document.on(_touchTap, ".mklbItem", function (e) {
  $(".mklbItem").blur();
  if (issetElement("#mkLightboxContainer")) {
    _html.addClass("overflow-hidden");
  } else {
    _html.removeClass("overflow-hidden");
  }
});

_document.on(_touchTap, "#overlay", function (e) {
  _html.removeClass("overflow-hidden");
});

_document.on(_touchTap, "#closeIconContainer", function (e) {
  _html.removeClass("overflow-hidden");
});

_document.keyup(function (e) {
  if (e.keyCode == 27) {
    $("#mkLightboxContainer").remove();
    _html.removeClass("overflow-hidden");
  }
});
});
