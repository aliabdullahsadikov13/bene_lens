function initMap() {
  const myLatlng = { lat: 41.25917113909974, lng: 69.37164740115719  };
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 12,
    center: myLatlng,
  });
  // Create the initial InfoWindow.
  let infoWindow = new google.maps.InfoWindow({
    content: "Click the map to get Lat/Lng!",
    position: myLatlng,
  });

  infoWindow.open(map);
  // Configure the click listener.
  map.addListener("click", (mapsMouseEvent) => {
    // Close the current InfoWindow.
    infoWindow.close();
    // Create a new InfoWindow.
    infoWindow = new google.maps.InfoWindow({
      position: mapsMouseEvent.latLng,
    });
    infoWindow.setContent(
      JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
    );
    infoWindow.open(map);
    lat = mapsMouseEvent.latLng.toJSON()['lat']
    lng = mapsMouseEvent.latLng.toJSON()['lng']
      console.log(lat,lng)
    document.getElementById('id_lat').value = lat
    document.getElementById('id_lang').value = lng
  });
}