from django.urls import path
from .views import homepageview,aboutpageview,katalogpageview,whyneopageview,faqview,lenscalculator,contactusview,\
    storeview,contactsuccessview,load_region_uz,load_stores_uz,filter_kategory,filter_changeTime,productDetailView

urlpatterns = [
    path('',homepageview,name='homepage'),
    path('about/',aboutpageview,name='aboutpage'),
    path('katalog/',katalogpageview,name='katalogpage'),
    path('katalog/<int:pk>',productDetailView.as_view(),name='productdetail'),
    path('why/',whyneopageview,name='whyneopage'),
    path('faq/', faqview, name='faqpage'),
    path('lenscalculator/', lenscalculator, name='lenscalculatorpage'),
    path('contactus/', contactusview, name='contactuspage'),
    path('contact-success/', contactsuccessview, name='contactsuccess'),
    path('store/', storeview, name='storepage'),
    path('ajax/districts-uz/', load_region_uz, name='load_districts_uz'),
    path('ajax/stores-uz/', load_stores_uz, name='load_stores_uz'),
    path('ajax/filter-kategory/', filter_kategory, name='filter_category_uz'),
    path('ajax/filter-changetime/', filter_changeTime, name='filter_changetime'),

]