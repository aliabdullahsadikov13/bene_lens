from django.forms import ModelForm
from products.models import Contact


class ContactForm(ModelForm):

    class Meta:
        model = Contact
        fields = "__all__"


