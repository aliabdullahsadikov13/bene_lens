from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from django.shortcuts import render, redirect
from django.views.generic import DetailView

from pages.forms import ContactForm
from products.models import Category, Region, District, Store, ChangineTimeLenses, Product, \
    Brand, WebInfoModel, good_with

from products.models import ClientReviews


def homepageview(request):
    brand = Brand.objects.all().order_by('brand_name')
    product = Product.objects.all()
    good_list = good_with.objects.all()
    color_list = ["#612D86", "#8EC642", "#EE642A"]
    all_list = zip(good_list, color_list)
    reviews = ClientReviews.objects.all()
    context = {
        'brand_name': brand,
        'all_products': product,
        "good_list": good_list,
        "all_list": all_list,
        "all_reviews": reviews,
    }

    return render(request, template_name='pages/home.html', context=context)


def katalogpageview(request):
    categories = Category.objects.all().order_by('categoryName')
    changing_time = ChangineTimeLenses.objects.all().order_by('time')
    products = Product.objects.all()
    paginator = Paginator(products, 9)  # Show 5contacts per page
    page_request_var = "page"
    page = request.GET.get(page_request_var)
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    context = {
        'categories': categories,
        'changing_time': changing_time,
        'products': products,
    }

    return render(request, template_name='pages/katalog.html', context=context)


class productDetailView(DetailView):
    model = Product
    template_name = 'pages/product_detail.html'


def aboutpageview(request):
    return render(request, template_name='pages/about.html')


def whyneopageview(request):
    return render(request, template_name='pages/why.html')


def faqview(request):
    return render(request, template_name='pages/faq.html')


def lenscalculator(request):
    return render(request, template_name='pages/lens-calculator.html')


def contactusview(request):
    form = ContactForm(request.POST or None)
    social = WebInfoModel.objects.all()
    if request.POST and form.is_valid():
        form.save()
        return redirect('contactsuccess')
    context = {
        "form": form,
        'social_websites': social,
    }

    return render(request, template_name='pages/contactus.html', context=context)


def storeview(request):
    stores = Store.objects.all()
    context = {
        'stores': stores,
    }
    return render(request, template_name='pages/store.html', context=context)


def contactsuccessview(request):
    social = WebInfoModel.objects.all()
    context = {
        'social_websites': social,
    }
    return render(request, template_name='pages/contact-success.html', context=context)


def load_region_uz(request):
    region_id = request.GET.get('region_id')
    districts_uz = District.objects.filter(region_id=region_id).order_by('name')
    return render(request, 'pages/load_district_uz.html', {'districts_uz': districts_uz})


def load_stores_uz(request):
    district_id = request.GET.get('district_id')
    district_uz = District.objects.filter(id=district_id).values('name_uz')[0]['name_uz']
    region_id = District.objects.filter(id=district_id).values('region_id')[0]['region_id']
    region_uz = Region.objects.filter(id=region_id).values('name_uz')[0]['name_uz']
    stores_uz = Store.objects.filter(district_id=district_id).order_by('store_name_uz')

    context = {
        'district_uz': district_uz,
        'region_uz': region_uz,
        'stores_uz': stores_uz
    }
    return render(request, 'pages/load_stores_uz.html', context)


def filter_kategory(request):
    category_id = request.GET.get('category_id')
    products = Product.objects.filter(category_id=category_id)
    context = {
        'filtered_products': products,
    }
    return render(request, 'pages/filtered_kategory.html', context)


def filter_changeTime(request):
    changetime_id = request.GET.get('changetime_id')
    products = Product.objects.filter(time_to_change_id=changetime_id)
    context = {
        "filtered_products": products,
    }
    return render(request, 'pages/filtered_changingtime.html', context)
