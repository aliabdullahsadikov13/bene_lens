from products.models import Region, WebInfoModel, Store

from products.models import ClientOnlineStores


def address(request):
    region_uz = Region.objects.all()
    online_stores = ClientOnlineStores.objects.all()
    stores = Store.objects.all()


    context = {
        'region_uz': region_uz,
        'online_stores': online_stores,
        'stores': stores,
    }

    return context